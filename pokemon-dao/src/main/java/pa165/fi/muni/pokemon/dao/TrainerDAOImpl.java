/*
 * Basic functionality methods for pokémon trainer.
 * Includes CRUD, data givers and two user input methods 
 * defeat and enterToLeague.
 */
package pa165.fi.muni.pokemon.dao;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;

@Repository("trainerDAO")
public class TrainerDAOImpl implements TrainerDAO {

    @Autowired
    @Qualifier("myEmf") // optional, just adds logic
    private EntityManagerFactory emf;

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public Trainer create(String name, Date birth) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        Trainer t = new Trainer();
        t.setName(name);
        t.setBirth(birth);

        em.persist(t);
        em.getTransaction().commit();
        em.close();

        return t;
    }

    @Override
    public void delete(long id) {
        EntityManager em = emf.createEntityManager();

        Query query;
        em.getTransaction().begin();

        query = em.createQuery("DELETE FROM Badge WHERE holder.id = :id")
                .setParameter("id", id);
        query.executeUpdate();

        query = em.createQuery("DELETE FROM Pokemon WHERE owner.id = :id")
                .setParameter("id", id);
        query.executeUpdate();

        query = em.createQuery("DELETE FROM Trainer WHERE id = :id")
                .setParameter("id", id);
        query.executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update(Trainer trainer) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Query query = em.createQuery("UPDATE Trainer SET name = :name, birth = :birth WHERE id = :id")
                .setParameter("name", trainer.getName())
                .setParameter("birth", trainer.getBirth())
                .setParameter("id", trainer.getId());
        query.executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public Trainer getTrainerById(Long id) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT t FROM Trainer t WHERE id = :id")
                .setParameter("id", id);

        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        Trainer t = (Trainer) query.getSingleResult();

        em.close();

        return t;
    }

    @Override
    public Trainer getTrainerByName(String name) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT t FROM Trainer t WHERE name = :name")
                .setParameter("name", name);

        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }

        Trainer t = (Trainer) query.getSingleResult();

        em.close();

        return t;
    }

    @Override
    public List<Trainer> getAllTrainers() {
        EntityManager em = emf.createEntityManager();
        List<Trainer> ts = em.createQuery("SELECT t FROM Trainer t ORDER BY name", Trainer.class)
                .getResultList();

        em.close();

        return ts;
    }

    @Override
    public int getCountBadges(Trainer trainer) {
        EntityManager em = emf.createEntityManager();
        Long bCount = em.createQuery("SELECT COUNT(b) FROM Badge b WHERE b.holder = :trainer", Long.class)
                .setParameter("trainer", trainer)
                .getSingleResult();

        em.close();

        return bCount.intValue();
    }

    @Override
    public Boolean hasBadge(Trainer trainer, Stadium stadium) {
        EntityManager em = emf.createEntityManager();

        Long bCount = em.createQuery("SELECT COUNT(b) FROM Badge b WHERE b.holder = :trainer AND b.stadium = :stadium", Long.class)
                .setParameter("trainer", trainer)
                .setParameter("stadium", stadium)
                .getSingleResult();

        em.close();

        return bCount > 0;
    }
}
