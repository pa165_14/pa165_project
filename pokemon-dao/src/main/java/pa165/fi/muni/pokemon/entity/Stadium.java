package pa165.fi.muni.pokemon.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author gabriel
 */
@Entity
public class Stadium {

    @Id
    @GeneratedValue
    private long id;

    @Column(unique=true)
    private String city;

    @Enumerated(EnumType.STRING)
    private Type type;

    @OneToOne(optional = false)
    private Trainer leader;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "stadium")
    private Set<Badge> sBadges = new HashSet<>();

    /**
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return type
     */
    public Type getType() {
        return type;
    }

    /**
     *
     * @param type type to set
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     *
     * @return leader who runs the stadium
     */
    public Trainer getLeader() {
        return leader;
    }

    /**
     *
     * @param leader stadium leader to set
     */
    public void setLeader(Trainer leader) {
        this.leader = leader;
    }

    /**
     *
     * @return badges previously issued by the stadium
     */
    public Set<Badge> getsBadges() {
        return sBadges;
    }

    /**
     *
     * @param sBadges set (previously) issued badges
     */
    public void setsBadges(Set<Badge> sBadges) {
        this.sBadges = sBadges;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stadium other = (Stadium) obj;
        return this.id == other.id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 29 * hash + Objects.hashCode(this.city);
        hash = 29 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public String toString() {
        return "Stadium in city: " + city + ", of type: " + type
                + ", with leader: " + leader;
    }
}
