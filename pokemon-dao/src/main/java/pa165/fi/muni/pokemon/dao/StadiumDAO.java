/*
 * Stadium interface
 */
package pa165.fi.muni.pokemon.dao;

import java.util.List;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author gabriel
 */
public interface StadiumDAO {

    /**
     * Creates stadium
     *
     * @param trainer trainer who runs the stadium
     * @param city name of city in which stadium lies
     * @param type type of stadium (ice, fire, ...)
     * @return created stadium
     */
    public Stadium create(Trainer trainer, String city, Type type);

    /**
     * Deletes stadium
     *
     * @param id id of stadium to be deleted
     */
    public void delete(long id);

    /**
     * Updates stadium
     *
     * @param stadium data of updated stadium
     */
    public void update(Stadium stadium);

    /**
     * Tries to assign trainer to stadium and returns true if it was successful.
     *
     * @param trainer trainer to assign to stadium
     * @param stadium stadium to be given the trainer
     * @return true if assigning was successful
     */
    public Boolean assignTrainerToStadium(Trainer trainer, Stadium stadium);

    /**
     * Returns stadium by its city in which lies.
     *
     * @param city name of city in which stadium lies
     * @return stadium if found
     */
    public Stadium getStadiumByCity(String city);

    /**
     * Returns list of all stadiums.
     *
     * @return list of all stadiums
     */
    public List<Stadium> getAllStadiums();

    /**
     * Returns list of all stadiums by given type.
     *
     * @param type type of stadium (ice, fire, ...)
     * @return list of stadiums with given type
     */
    public List<Stadium> getAllStadiumsByType(Type type);

}
