/*
 * Trainer interface
 */
package pa165.fi.muni.pokemon.dao;

import java.util.Date;
import java.util.List;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;

/**
 *
 * @author Kuba
 */
public interface TrainerDAO {

    /**
     * Creates trainer
     *
     * @param name name of trainer
     * @param birth birth date of trainer
     * @return created trainer
     */
    public Trainer create(String name, Date birth);

    /**
     * Deletes trainer
     *
     * @param id id of trainer to be deleted
     */
    public void delete(long id);

    /**
     * Updates trainer
     *
     * @param trainer data of updated trainer
     */
    public void update(Trainer trainer);

    /**
     * Returns trainer by given id or null if not found.
     *
     * @param id id of trainer to be found
     * @return trainer if found
     */
    public Trainer getTrainerById(Long id);

    /**
     * Returns trainer by given name or null if not found.
     *
     * @param name name of trainer to be found
     * @return trainer if found
     */
    public Trainer getTrainerByName(String name);

    /**
     * Returns list of all trainers in database.
     *
     * @return list of all trainers
     */
    public List<Trainer> getAllTrainers();

    /**
     * Returns count of badges trainer already obtained.
     *
     * @param trainer trainer to count badges
     * @return count of badges trainer has
     */
    public int getCountBadges(Trainer trainer);

    /**
     * Returns true if trainer already has badge from this stadium.
     *
     * @param trainer trainer to test
     * @param stadium stadium to test
     * @return true if given trainer has badge from given stadium
     */
    public Boolean hasBadge(Trainer trainer, Stadium stadium);

}
