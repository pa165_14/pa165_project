package pa165.fi.muni.pokemon.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Milan
 */
@Entity
public class Badge {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private Trainer holder;

    @ManyToOne
    private Stadium stadium;

    /**
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return trainer holding the badge
     */
    public Trainer getHolder() {
        return holder;
    }

    /**
     *
     * @param holder trainer holding the badge to set
     */
    public void setHolder(Trainer holder) {
        this.holder = holder;
    }

    /**
     *
     * @return stadium issuing this badge
     */
    public Stadium getStadium() {
        return stadium;
    }

    /**
     *
     * @param stadium set stadium issuing this badge
     */
    public void setStadium(Stadium stadium) {
        this.stadium = stadium;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Badge other = (Badge) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "Badge{" + "id=" + id + ", holder=" + holder + ", stadium=" + stadium + '}';
    }
}
