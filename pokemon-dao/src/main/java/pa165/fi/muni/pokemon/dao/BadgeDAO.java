/*
 * Badge interface
 */
package pa165.fi.muni.pokemon.dao;

import java.util.List;
import pa165.fi.muni.pokemon.entity.Badge;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Stadium;

/**
 *
 * @author Milan
 */
public interface BadgeDAO {

    /**
     * Creates badge
     *
     * @param trainer which trainer will own the badge
     * @param stadium what stadium has the trainer defeated
     * @return created badge
     */
    public Badge create(Trainer trainer, Stadium stadium);

    /**
     * Deletes badge
     *
     * @param id id of badge to be deleted
     */
    public void delete(long id);

    /**
     * Updates badge
     *
     * @param badge data of updated badge
     */
    public void update(Badge badge);

    /**
     * Returns all badges you can obtain.
     *
     * @param holder trainer
     * @return list of all badges
     */
    public List<Badge> getAllTrainerBadges(Trainer holder);

    /**
     * Returns all types of badges you can obtain.
     *
     * @return list of all badges
     */
    public List<Badge> getAllBadges();

    /**
     * Returns count of badges you can obtain.
     *
     * @return count of all badges
     */
    public int countAllBadges();

}
