package pa165.fi.muni.pokemon.dao;

import java.util.List;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pa165.fi.muni.pokemon.entity.Badge;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;

/**
 *
 * @author Milan
 */
@Repository("badgeDAO")
public class BadgeDAOImpl implements BadgeDAO {

    @Autowired
    private EntityManagerFactory emf;

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public Badge create(Trainer trainer, Stadium stadium) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        Badge badge = new Badge();
        badge.setHolder(trainer);
        badge.setStadium(stadium);

        em.persist(badge);
        em.getTransaction().commit();
        em.close();

        return badge;
    }

    @Override
    public void delete(long id) {
        EntityManager em = emf.createEntityManager();

        Query query;
        em.getTransaction().begin();

        query = em.createQuery("DELETE FROM Badge WHERE id = :id")
                .setParameter("id", id);
        query.executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update(Badge badge) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Query query = em.createQuery("UPDATE Badge SET stadium = :stadium, holder = :holder WHERE id = :id")
                .setParameter("stadium", badge.getStadium())
                .setParameter("holder", badge.getHolder())
                .setParameter("id", badge.getId());
        query.executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<Badge> getAllBadges() {
        EntityManager em = emf.createEntityManager();
        List<Badge> badgeList = em.createQuery("SELECT b FROM Badge b ORDER BY stadium", Badge.class)
                .getResultList();

        em.close();
        return badgeList;
    }

    @Override
    public int countAllBadges() {
        EntityManager em = emf.createEntityManager();
        Long bCount = em.createQuery("SELECT COUNT(b) FROM Badge b", Long.class)
                .getSingleResult();

        em.close();

        return bCount.intValue();
    }

    @Override
    public List<Badge> getAllTrainerBadges(Trainer holder) {
        EntityManager em = emf.createEntityManager();
        List<Badge> badgeList = em.createQuery("SELECT b FROM Badge b WHERE holder =:holder", Badge.class)
                .setParameter("holder", holder)
                .getResultList();

        em.close();
        return badgeList;
    }
}
