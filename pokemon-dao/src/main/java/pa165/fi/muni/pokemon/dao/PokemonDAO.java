/*
 * Pokemon interface
 */
package pa165.fi.muni.pokemon.dao;

import java.util.List;
import pa165.fi.muni.pokemon.entity.Pokemon;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Petr
 */
public interface PokemonDAO {

    public Pokemon addPokemon(Pokemon pokemon);

    /**
     * Creates pokémon
     *
     * @param trainer which will own created pokémon
     * @param name type name of the pokémon (e.g. Pikachu)
     * @param nickname nickname of the pokémon
     * @param type type of the pokémon (ice, fire, etc.)
     * @param lvl level of the pokémon
     * @return created pokémon
     */
    public Pokemon create(Trainer trainer, String name, String nickname, Type type, int lvl);

    /**
     * Deletes pokémon
     *
     * @param id id of pokémon to be deleted
     */
    public void delete(long id);

    /**
     * Updates pokémon
     *
     * @param pokemon data of updated pokémon
     */
    public void update(Pokemon pokemon);

    /**
     * Returns pokémon by its id
     *
     * @param id id of pokémon to be found
     * @return pokémon if found
     */
    public Pokemon getPokemonById(Long id);

    /**
     * Returns pokémon by its nickname
     *
     * @param name name of pokémon to be found
     * @return pokémon if found
     */
    public Pokemon getPokemonByNickname(String name);

    /**
     * Returns list of pokémons of a trainer
     *
     * @param trainer owner of pokémons to be found
     * @return list of pokémons found
     */
    public List<Pokemon> getPokemonsOfTrainer(Trainer trainer);

    /**
     * Returns list of all pokémons
     *
     *
     * @return list of pokémons found
     */
    public List<Pokemon> getAllPokemons();
}
