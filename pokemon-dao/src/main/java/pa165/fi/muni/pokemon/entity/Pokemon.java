package pa165.fi.muni.pokemon.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Petr
 */
@Entity
public class Pokemon {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = true)
    private String nickname;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Column(nullable = false)
    private int lvl;

    @ManyToOne
    private Trainer owner = null;

    /**
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     *
     * @param nickname nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     *
     * @return type
     */
    public Type getType() {
        return type;
    }

    /**
     *
     * @param type type to set
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     *
     * @return level
     */
    public int getLvl() {
        return lvl;
    }

    /**
     *
     * @param lvl level to set
     */
    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    /**
     *
     * @return trainer owning the pokémon
     */
    public Trainer getOwner() {
        return owner;
    }

    /**
     *
     * @param owner trainer owning the pokémon to set
     */
    public void setOwner(Trainer owner) {
        this.owner = owner;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pokemon other = (Pokemon) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "Pokémon: " + name + ", nickname: " + nickname
                + ", level: " + lvl + ", trainer: " + owner;
    }
}
