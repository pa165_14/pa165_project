package pa165.fi.muni.pokemon.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pa165.fi.muni.pokemon.entity.Pokemon;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Petr
 */
@Repository("pokemonDAO")
public class PokemonDAOImpl implements PokemonDAO {

    @Autowired
    private EntityManagerFactory emf;

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public Pokemon addPokemon(Pokemon pokemon) {
        if (pokemon == null) {
            throw new IllegalArgumentException("Pokemon cannot be null!");
        }
        return create(pokemon.getOwner(), pokemon.getName(), pokemon.getNickname(), pokemon.getType(), pokemon.getLvl());
    }

    @Override
    public Pokemon create(Trainer owner, String name, String nickname, Type type, int lvl) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Pokemon pokemon = new Pokemon();
        pokemon.setName(name);
        pokemon.setNickname(nickname);
        pokemon.setLvl(lvl);
        pokemon.setType(type);
        pokemon.setOwner(owner);
        em.persist(pokemon);
        em.getTransaction().commit();
        em.close();

        return pokemon;
    }

    @Override
    public void delete(long id) {
        EntityManager em = emf.createEntityManager();

        Query query;
        em.getTransaction().begin();

        query = em.createQuery("DELETE FROM Pokemon WHERE id = :id")
                .setParameter("id", id);
        query.executeUpdate();

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void update(Pokemon pokemon) {
        EntityManager em = emf.createEntityManager();

        // should we be able to change name of pokémon?
        em.getTransaction().begin();
        Query query = em.createQuery("UPDATE Pokemon SET name = :name, nickname = :nickname, "
                + "type = :type, lvl = :lvl, owner = :owner WHERE id = :id")
                .setParameter("name", pokemon.getName())
                .setParameter("nickname", pokemon.getNickname())
                .setParameter("type", pokemon.getType())
                .setParameter("lvl", pokemon.getLvl())
                .setParameter("owner", pokemon.getOwner())
                .setParameter("id", pokemon.getId());
        query.executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public Pokemon getPokemonById(Long id) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT p FROM Pokemon p WHERE id = :id")
                .setParameter("id", id);

        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        Pokemon p = (Pokemon) query.getSingleResult();

        em.close();

        return p;
    }

    @Override
    public Pokemon getPokemonByNickname(String name) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT p FROM Pokemon p WHERE nickname = :name")
                .setParameter("name", name);

        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }

        Pokemon p = (Pokemon) query.getSingleResult();

        em.close();

        return p;
    }

    @Override
    public List<Pokemon> getPokemonsOfTrainer(Trainer trainer) {
        EntityManager em = emf.createEntityManager();
        List<Pokemon> results = em.createQuery("SELECT p FROM Pokemon p WHERE owner = :trainer", Pokemon.class)
                .setParameter("trainer", trainer)
                .getResultList();

        em.close();

        return results;
    }

    @Override
    public List<Pokemon> getAllPokemons() {
        EntityManager em = emf.createEntityManager();
        List<Pokemon> results = em.createQuery("SELECT p FROM Pokemon p ORDER BY owner", Pokemon.class)
                .getResultList();

        em.close();

        return results;
    }
}
