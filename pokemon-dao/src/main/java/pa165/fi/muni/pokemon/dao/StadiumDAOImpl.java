package pa165.fi.muni.pokemon.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author gabriel
 */
@Repository("stadiumDAO")
public class StadiumDAOImpl implements StadiumDAO {

    @Autowired
    private EntityManagerFactory emf;

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public Stadium create(Trainer trainer, String city, Type type) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        Stadium stadium = new Stadium();
        stadium.setCity(city);
        stadium.setLeader(trainer);
        stadium.setType(type);

        em.persist(stadium);
        em.getTransaction().commit();
        em.close();

        return stadium;
    }

    @Override
    public void delete(long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Stadium stadium = em.find(Stadium.class, id);
        em.remove(stadium);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void update(Stadium stadium) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Stadium st = em.find(Stadium.class, stadium.getId());
        st.setCity(stadium.getCity());
        st.setLeader(stadium.getLeader());
        st.setType(stadium.getType());
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public Stadium getStadiumByCity(String city) {
        EntityManager em = emf.createEntityManager();

        Query query = em.createQuery("SELECT s FROM Stadium s WHERE city = :city")
                .setParameter("city", city);

        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }

        Stadium stadium = (Stadium) query.getSingleResult();

        em.close();

        return stadium;
    }

    @Override
    public List<Stadium> getAllStadiums() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<Stadium> stadiumList = em.createQuery("SELECT s FROM Stadium s ORDER BY city", Stadium.class)
                .getResultList();

        em.close();

        return stadiumList;
    }

    @Override
    public List<Stadium> getAllStadiumsByType(Type type) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<Stadium> stadiumList;
        stadiumList = em.createQuery("SELECT s FROM Stadium s WHERE type = :type", Stadium.class)
                .setParameter("type", type)
                .getResultList();

        em.close();

        return stadiumList;
    }

    @Override
    public Boolean assignTrainerToStadium(Trainer trainer, Stadium stadium) {
        if (trainer.getStadium() != null) {
            return false;
        }

        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        if (stadium.getLeader() != null) {
            Trainer leader = stadium.getLeader();
            leader.setStadium(null);
        }

        stadium.setLeader(trainer);

        em.getTransaction().commit();
        em.close();

        return true;
    }
}
