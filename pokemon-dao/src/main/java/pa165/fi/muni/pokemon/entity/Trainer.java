package pa165.fi.muni.pokemon.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Kuba
 */
@Entity
public class Trainer implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    private String name;

    @Temporal(TemporalType.DATE)
    private Date birth;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner")
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    private Set<Pokemon> pokemons = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "holder")
    private Set<Badge> badges = new HashSet<>();

    //Needed?
    @OneToOne(mappedBy = "leader")
    private Stadium stadium;

    /**
     *
     * @return trainer id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return trainer id
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return birth
     */
    public Date getBirth() {
        return birth;
    }

    /**
     *
     * @param birth birth to set
     */
    public void setBirth(Date birth) {
        this.birth = birth;
    }

    /**
     *
     * @return set of trainer pokemons
     */
    public Set<Pokemon> getPokemons() {
        return pokemons;
    }

    /**
     *
     * @param pokemons pokemons to assign to trainer
     */
    public void setPokemons(Set<Pokemon> pokemons) {
        this.pokemons = pokemons;
    }

    /**
     *
     * @return trainer badges
     */
    public Set<Badge> getBadges() {
        return badges;
    }

    /**
     *
     * @param badges badges to assign to trainer
     */
    public void setBadges(Set<Badge> badges) {
        this.badges = badges;
    }

    /**
     *
     * @return stadium if trainer has one
     */
    public Stadium getStadium() {
        return stadium;
    }

    /**
     *
     * @param stadium stadium to set
     */
    public void setStadium(Stadium stadium) {
        this.stadium = stadium;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Trainer other = (Trainer) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "Trainer: " + name + ", born: " + birth;
    }
}
