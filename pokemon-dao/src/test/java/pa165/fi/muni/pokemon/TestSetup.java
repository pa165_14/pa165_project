/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import pa165.fi.muni.pokemon.entity.Badge;
import pa165.fi.muni.pokemon.entity.Pokemon;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Kuba
 */
public class TestSetup {

    /**
     *
     */
    public static long ashId;

    /**
     *
     */
    public static long garryId;

    /**
     *
     */
    public static long pikachuId;
    // later introduce method and delete this variable

    /**
     *
     */
    public static Stadium stadiumWinter;

    /**
     *
     */
    public static Stadium stadium;

    // code merged
    /**
     *
     */
    public static Trainer trainer1;

    /**
     *
     */
    public static Trainer trainer2;

    /*
     * So we have Garry, who runs his ice stadium in Winterfell
     * Ash doesn't have stadium, but two badges from both
     * Saruman runs his steel stadium in Isengard
     *
     * @param emf
     */
    public static void init(EntityManagerFactory emf) {

        EntityManager em = emf.createEntityManager();
        Calendar cal = Calendar.getInstance();

        Trainer ash = new Trainer();
        ash.setName("Ash");
        cal.set(1997, 5, 22);
        ash.setBirth(cal.getTime());

        Trainer garry = new Trainer();
        garry.setName("Garry");
        cal.set(1997, 9, 29);
        garry.setBirth(cal.getTime());

        Stadium st1 = new Stadium();
        st1.setCity("Winterfell");
        st1.setLeader(garry);
        st1.setType(Type.ICE);

        Trainer t1 = new Trainer();
        t1.setName("Saruman");
        cal.set(-9500, 0, 1);
        t1.setBirth(cal.getTime());

        Stadium st2 = new Stadium();
        st2.setCity("Isengard");
        st2.setLeader(t1);
        st2.setType(Type.STEEL);
        t1.setStadium(st2);

        Badge bAsh1 = new Badge();
        bAsh1.setHolder(ash);
        bAsh1.setStadium(st1);

        Badge bAsh2 = new Badge();
        bAsh2.setHolder(ash);
        bAsh2.setStadium(st2);

        // back connection to ash's badges
        Set<Badge> bAshSetFirst = new HashSet<>();
        Set<Badge> bAshSetSecond = new HashSet<>();
        Set<Badge> bAshSetBoth = new HashSet<>();

        bAshSetFirst.add(bAsh1);
        bAshSetSecond.add(bAsh2);
        bAshSetBoth.add(bAsh1);
        bAshSetBoth.add(bAsh2);

        st1.setsBadges(bAshSetFirst);
        st2.setsBadges(bAshSetSecond);
        ash.setBadges(bAshSetBoth);

        Pokemon p1 = new Pokemon();
        p1.setName("Pikachu");
        p1.setNickname("Pikachu");
        p1.setType(Type.ELECTRIC);
        p1.setLvl(80);
        p1.setOwner(ash);

        Pokemon p2 = new Pokemon();
        p2.setName("Charizard");
        p2.setNickname("Charmander");
        p2.setType(Type.FIRE);
        p2.setLvl(46);
        p2.setOwner(ash);

        Pokemon p3 = new Pokemon();
        p3.setName("Articuno");
        p3.setName("Nedd");
        p3.setType(Type.ICE);
        p3.setLvl(64);
        p3.setOwner(garry);

        Set<Pokemon> ashPokes = new HashSet<>();
        ashPokes.add(p1);
        ashPokes.add(p2);
        ash.setPokemons(ashPokes);

        Set<Pokemon> garryPokes = new HashSet<>();
        garryPokes.add(p3);
        garry.setPokemons(garryPokes);

        em.getTransaction().begin();
        em.persist(ash);
        em.persist(garry);
        em.persist(t1);
        em.persist(st1);
        em.persist(st2);
        em.persist(p1);
        em.persist(p2);
        em.persist(p3);
        em.persist(bAsh1);
        em.persist(bAsh2);

        // Ash and Garry have ids visible in this class
        ashId = ash.getId();
        garryId = garry.getId();
        stadiumWinter = st1;
        stadium = st2;
        pikachuId = p1.getId();

        trainer1 = ash;
        trainer2 = garry;

        em.getTransaction().commit();
        em.close();
    }
}
