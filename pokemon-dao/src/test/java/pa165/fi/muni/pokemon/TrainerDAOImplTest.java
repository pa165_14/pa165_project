package pa165.fi.muni.pokemon;

import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pa165.fi.muni.pokemon.dao.StadiumDAO;

import pa165.fi.muni.pokemon.dao.TrainerDAO;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;

/**
 *
 * @author Petr
 */
@ContextConfiguration(locations = "/META-INF/persistence-beans-app.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TrainerDAOImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private TrainerDAO trainerDAO;

    @Autowired
    private StadiumDAO stadiumDAO;

    @PersistenceUnit
    public EntityManagerFactory emf;

    private long ashId;
    private long garryId;

    @BeforeMethod
    public void setup() {
        TestSetup.init(emf);

        // Ash and Garry have ids visible in this class
        ashId = TestSetup.ashId;
        garryId = TestSetup.garryId;
    }

    @Test
    public void createTest() {
        Calendar cal = Calendar.getInstance();

        cal.set(1960, 2, 8);

        Trainer oak = trainerDAO.create("Prof. Oak", cal.getTime());

        Assert.assertNotNull(oak.getName());
    }

    @Test
    public void updateTest() {
        Trainer t1 = trainerDAO.getTrainerById(garryId);
        String newName = t1.getName() + " Oak";
        t1.setName(newName);
        trainerDAO.update(t1);

        Trainer t2 = trainerDAO.getTrainerById(garryId);
        Assert.assertEquals(t2.getName(), "Garry Oak");
    }

    @Test
    public void deleteTest() {
        Trainer t1 = trainerDAO.getTrainerByName("Ash");
        trainerDAO.delete(t1.getId());

        Trainer t2 = trainerDAO.getTrainerByName("Ash");
        Assert.assertNull(t2);
    }

    @Test
    public void getAllTrainersTest() {
        List<Trainer> trainers = trainerDAO.getAllTrainers();
        Assert.assertEquals(trainers.get(0).getName(), "Ash");
        Assert.assertEquals(trainers.get(1).getName(), "Garry");

        Stadium stadium = stadiumDAO.getStadiumByCity("Winterfell");
        Assert.assertEquals(trainers.get(1).getStadium(), stadium);
    }
}
