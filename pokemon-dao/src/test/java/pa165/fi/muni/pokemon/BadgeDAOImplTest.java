package pa165.fi.muni.pokemon;

import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pa165.fi.muni.pokemon.dao.BadgeDAO;
import pa165.fi.muni.pokemon.entity.Badge;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;

/**
 *
 * @author Milan
 */
@ContextConfiguration(locations = "/META-INF/persistence-beans-app.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BadgeDAOImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private BadgeDAO badgeDAO;

    @PersistenceUnit
    public EntityManagerFactory emf;

    private Stadium stadium1;
    private Trainer holder1;
    private Trainer holder2;

    @BeforeMethod
    public void setup() {
        TestSetup.init(emf);

        stadium1 = TestSetup.stadium;
        holder1 = TestSetup.trainer1;
        holder2 = TestSetup.trainer2;

    }

    @Test
    public void createTest() {

        Badge b = badgeDAO.create(holder1, stadium1);

        Assert.assertEquals(b.getStadium(), stadium1);
    }

    @Test
    public void updateTest() {
        Badge b1 = badgeDAO.create(holder2, stadium1);
        b1.setHolder(holder1);
        badgeDAO.update(b1);

        Assert.assertEquals(b1.getHolder(), holder1);
    }

    @Test
    public void deleteTest() {
        List<Badge> badges2 = badgeDAO.getAllBadges();

        Stadium stad = badges2.get(0).getStadium();
        badgeDAO.delete(badges2.get(0).getId());

        badges2 = badgeDAO.getAllBadges();
        for (Badge badges21 : badges2) {
            Assert.assertNotSame(badges21.getStadium().getCity(), stad.getCity());
        }
    }

    @Test
    public void getAllBadgesTest() {
        List<Badge> badges = badgeDAO.getAllBadges();
        Assert.assertEquals(badges.get(0).getHolder().getName(), holder1.getName());
        Assert.assertEquals(badges.get(1).getHolder().getName(), holder1.getName());
        Assert.assertEquals(badges.size(), 2);
    }

    @Test
    public void countAllBadgesTest() {
        int bcount = badgeDAO.countAllBadges();
        Assert.assertEquals(bcount, 2);
    }
}
