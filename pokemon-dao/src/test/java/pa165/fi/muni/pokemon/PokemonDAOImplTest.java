package pa165.fi.muni.pokemon;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pa165.fi.muni.pokemon.dao.PokemonDAO;
import pa165.fi.muni.pokemon.dao.TrainerDAO;
import pa165.fi.muni.pokemon.entity.Pokemon;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Kuba
 */
@ContextConfiguration(locations = "/META-INF/persistence-beans-app.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PokemonDAOImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private TrainerDAO trainerDAO;

    @Autowired
    private PokemonDAO pokemonDAO;

    @PersistenceUnit
    public EntityManagerFactory emf;

    private long ashId;
    private long garryId;
    private long pikachuId;

    @BeforeMethod
    public void setup() {
        TestSetup.init(emf);

        // Ash and Garry have ids visible in this class
        ashId = TestSetup.ashId;
        garryId = TestSetup.garryId;
        pikachuId = TestSetup.pikachuId;
    }

    /**
     * Tests create, getTrainerById and getPokemonByNickname
     */
    @Test
    public void createTest() {
        Trainer garry = trainerDAO.getTrainerById(garryId);

        Pokemon dewgong = pokemonDAO.create(garry, "Dewgong", "Bran", Type.ICE, 61);

        Pokemon dewTest = pokemonDAO.getPokemonByNickname("Bran");
        Assert.assertEquals(dewgong, dewTest);
    }

    @Test
    public void updateTest() {
        Pokemon pikachu = pokemonDAO.getPokemonById(pikachuId);
        pikachu.setType(Type.FIRE);
        pokemonDAO.update(pikachu);

        Pokemon updated = pokemonDAO.getPokemonById(pikachuId);
        Assert.assertEquals(updated.getType(), Type.FIRE);
    }

    @Test
    public void deleteTest() {
        Pokemon pikachu = pokemonDAO.getPokemonByNickname("Pikachu");
        pokemonDAO.delete(pikachu.getId());

        Pokemon deleted = pokemonDAO.getPokemonByNickname("Pikachu");
        Assert.assertNull(deleted);
    }
}
