package pa165.fi.muni.pokemon;

import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pa165.fi.muni.pokemon.dao.StadiumDAO;
import pa165.fi.muni.pokemon.dao.TrainerDAO;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author gabriel
 */
@ContextConfiguration(locations = "/META-INF/persistence-beans-app.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StadiumDAOImplTest extends AbstractTestNGSpringContextTests {

    @PersistenceUnit
    public EntityManagerFactory emf;

    @Autowired
    private TrainerDAO trainerDao;

    @Autowired
    private StadiumDAO stadiumDao;

    private long ashId;
    private long garryId;
    // later introduce method and delete this variable
    private Stadium stadium;
    private Stadium stadiumWinter;

    @BeforeMethod
    public void setup() {
        TestSetup.init(emf);

        // Ash and Garry have ids visible in this class        
        ashId = TestSetup.ashId;
        garryId = TestSetup.garryId;
        stadium = TestSetup.stadium;
        stadiumWinter = TestSetup.stadiumWinter;
    }

    @Test
    public void testCreate() {
        System.out.println("Testing create method");
        Trainer trainer = trainerDao.getTrainerById(ashId);
        String city = "Tralala";
        Type type = Type.ROCK;

        Stadium result = stadiumDao.create(trainer, city, type);
        Stadium expResult = stadiumDao.getStadiumByCity(city);
        Assert.assertEquals(result, expResult);
    }

    /**
     * Test of delete method, of class StadiumDAOImpl.
     */
    @Test
    public void testDelete() {
        System.out.println("Testing delete method");
        Trainer trainer = trainerDao.getTrainerById(ashId);
        String city = "Tralala";
        Type type = Type.ROCK;
        Stadium result = stadiumDao.create(trainer, city, type);
        stadiumDao.delete(result.getId());

        Stadium test = stadiumDao.getStadiumByCity("Tralala");
        Assert.assertNull(test);
    }

    /**
     * Test of update method, of class StadiumDAOImpl.
     */
    @Test
    public void testUpdate() {
        System.out.println("Testing update method.");
        Stadium stadiumTest = stadiumDao.getStadiumByCity("Winterfell");
        stadiumTest.setType(Type.BUG);
        stadiumDao.update(stadiumTest);
        Assert.assertEquals(stadiumTest, stadiumDao.getStadiumByCity("Winterfell"));
    }

    /**
     * Test of getAllStadiums method, of class StadiumDAOImpl.
     */
    @Test
    public void testGetAllStadiums() {
        System.out.println("getAllStadiums");
        List result = stadiumDao.getAllStadiums();
        Assert.assertNotNull(result);
    }

    /**
     * Test of getAllStadiumsByType method, of class StadiumDAOImpl.
     */
    @Test
    public void testGetAllStadiumsByType() {
        System.out.println("getAllStadiumsByType");
        Type type = Type.ICE;
        List result = stadiumDao.getAllStadiumsByType(type);
        Assert.assertNotNull(result);
    }

    /**
     * Test of getStadiumByCity method, of class StadiumDAOImpl.
     */
    @Test
    public void testGetStadiumByCity() {
        System.out.println("getStadiumByCity");
        String city = "Winterfell";
        Stadium result = stadiumDao.getStadiumByCity(city);
        Assert.assertNotNull(result);
    }

    /**
     * Test of assignTrainerToStadium method, of class StadiumDAOImpl.
     */
    @Test
    public void testAssignTrainerToStadium() {
        Trainer garry = trainerDao.getTrainerById(garryId);
        Trainer ash = trainerDao.getTrainerById(ashId);

        Assert.assertEquals(stadiumWinter.getLeader().getId(), garryId);

        // try to assign garry to second stadium
        Assert.assertFalse(stadiumDao.assignTrainerToStadium(garry, stadium));

        // Ash is going to kick garry out of Winterfell
        stadiumDao.assignTrainerToStadium(ash, stadiumWinter);
        Stadium stadiumReloaded = stadiumDao.getStadiumByCity(stadiumWinter.getCity());
        Assert.assertEquals(stadiumReloaded.getId(), ashId);
    }
}
