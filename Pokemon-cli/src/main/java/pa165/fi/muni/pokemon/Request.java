/*
 * HTTP request class
 * Import from my Android project 
 * (so it doesn't have to be necessarily java friendly)
 */
package pa165.fi.muni.pokemon;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Kuba
 */
public class Request {

    public static String makeRequest(String url, List<NameValuePair> nameValuePair) {
        return makeRequest(url, nameValuePair, "post");
    }
    
    public static String makeRequest(String url, List<NameValuePair> nameValuePair, String method) {
        HttpPost httppost = null;
        HttpGet httpget = null;
        HttpPut httpput = null;
        HttpDelete httpdelete = null;
        HttpResponse response = null;
        
        /* Create a new HttpClient and Post Header */
        HttpClient httpclient = new DefaultHttpClient();
        String credentials = "rest:rest";
        String authString = "";

        try {
            authString = Base64.getEncoder().encodeToString(credentials.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, e);
        }

        if (method.equals("post")) {
            httppost = new HttpPost(url);
            httppost.addHeader("Authorization", authString);
        }
        if (method.equals("get")) {
            httpget = new HttpGet(url);
            httpget.addHeader("Authorization", authString);
        }
        if (method.equals("put")) {
            httpput = new HttpPut(url);
            httpput.addHeader("Authorization", authString);
        }
        if (method.equals("delete")) {
            httpdelete = new HttpDelete(url);
            httpdelete.addHeader("Authorization", authString);
        }
        String responseBody;
        
        try {
            if (nameValuePair != null) {
                if (method.equals("post") && httppost != null) {
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                }
                if (method.equals("put") && httpput != null) {
                    httpput.setEntity(new UrlEncodedFormEntity(nameValuePair));
                }
            }

            /* Execute HTTP Post Request */
            if (method.equals("post")) {
                response = httpclient.execute(httppost);
            }
            if (method.equals("get")) {
                response = httpclient.execute(httpget);
            }
            if (method.equals("put")) {
                response = httpclient.execute(httpput);
            }
            if (method.equals("delete")) {
                response = httpclient.execute(httpdelete);
            }
            
            if (response == null) {
                responseBody = "Unexpected client error. Please try again later.";
            } else if (response.getStatusLine().getStatusCode() == 403) {
                responseBody = "Unauthorized access.";
            } else if (response.getStatusLine().getStatusCode() == 404) {
                responseBody = "Server is unavailable.";
            } else {
                HttpEntity entity = response.getEntity();
                responseBody = EntityUtils.toString(entity);
            }

        } catch (ClientProtocolException e) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, e);
            responseBody = "Unexpected client error. Please try again later.";
        } catch (IOException e) {
            responseBody = "Unfortunately server is not available. Please try again later.";
        }

        return responseBody;
    }
}
