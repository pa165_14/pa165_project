package pa165.fi.muni.pokemon;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Kuba
 */
public class Commands {

    /* mode to set for inputting commands (basic, trainer, stadium) */
    private int inputMode;
    private String trainerId = "";
    private String stadiumCity = "";

    /* tree of commands starting at given words */
    private Map<String, List<String>> showCommands;

    private Map<String, String> commandCallbacks;
    private Map<String, Integer> commandModes;
    private Map<String, String> commandHelpers;

    /*
     * Not absolutely necessary, being used for keeping order when only
     * question mark is typed.
     */
    private List<String> allCommandsOrdered;

    public Commands() {
        inputMode = 0;              // basic mode set
        showCommands = new HashMap<>();
        commandCallbacks = new HashMap<>();
        commandModes = new HashMap<>();
        commandHelpers = new HashMap<>();
        allCommandsOrdered = new ArrayList<>();
    }

    /*
     * returns all combinations of one commands
     *
     * trainer, trainer add, trainer add name
     */
    public void getTokenizationCombinations(String line, int mode) {
        StringTokenizer st = new StringTokenizer(line, ",");
        String tmpLine;
        String callback;
        String helper = "";
        String previous = "";

        // splits arguments delimited by comma - command and its callback
        if (st.hasMoreTokens()) {
            tmpLine = st.nextToken().trim();
        } else {
            return;
        }

        if (st.hasMoreTokens()) {
            callback = st.nextToken().trim();
        } else {
            return;
        }
        
        /* extract helper */
        if (st.hasMoreTokens()) {
            helper = st.nextToken().trim();
        }

        line = tmpLine;
        st = new StringTokenizer(line);

        while (st.hasMoreTokens()) {
            String word = st.nextToken();
            word = word.replace("_", " ");
            
            previous += word;

            List<String> showCommandsArray = showCommands.get(previous);
            if (showCommandsArray == null) {
                showCommandsArray = new ArrayList<>();
            }
            showCommandsArray.add(line);
            showCommands.put(previous, showCommandsArray);

            /*
             * Command can be called only if it's unique so we don't have to
             * be worried about this map if keys get replaced occasionally.
             */
            commandCallbacks.put(previous, callback);
            commandModes.put(previous, mode);
            commandHelpers.put(previous, helper);

            previous += " ";
        }

        /* store ordered record */
        allCommandsOrdered.add(line);
    }

    /*
     * Loads configuration file
     */
    public void loadCommandsFromFile(String fileName) {
        /* used as switch for basic / trainer / stadium commands */
        int mode = 0;

        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));

            String line;

            while ((line = br.readLine()) != null) {

                if (line.isEmpty()) {
                    continue;
                }

                if (line.equals("# trainer interface")) {
                    mode = 1;
                }

                if (line.equals("# stadium interface")) {
                    mode = 2;
                }

                if (line.charAt(0) == '#') {
                    continue;
                }

                getTokenizationCombinations(line, mode);

            }
            br.close();

        } catch (IOException ex) {
            Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Loading command configuration failed. Please report this incident.");
        }
    }

    /* prints out list of commands from list */
    private void printCommands(List<String> commandList) {
        for (String command : commandList) {
            int mode = commandModes.get(command);
            if (mode == inputMode) {
                System.out.print(command);
                if (!commandHelpers.get(command).isEmpty()) {
                    System.out.print("\t# " + commandHelpers.get(command));
                }
                System.out.println();
            }
        }
        System.out.println("exit");
    }

    /* finds last occurence of character in a string */
    private int rightIndexOf(String myString, char toFind) {
        int rightPosition = 0;

        for (int i = myString.length() - 1; i >= 0; i--) {
            if (myString.charAt(i) == toFind) {
                rightPosition = i;
                break;
            }
        }

        return rightPosition;
    }

    /* parses one input line */
    public Boolean parseCommand(String line) {
        if (line.isEmpty()) {
            return true;
        }

        /* you don't want to play anymore? */
        if (line.equals("exit")) {
            if (inputMode == 0) {
                return false;
            } else {            // returning to basic mode
                trainerId = "";
                stadiumCity = "";
                inputMode = 0;
            }

            return true;
        }

        /*
         * Somebody asks for help?
         * Let's strip question mark first.
         */
        if (line.charAt(line.length() - 1) == '?') {
            line = line.substring(0, line.length() - 1);
            line = line.trim();

            /* only question mark was typed, all commands should be printed out */
            if (line.isEmpty()) {
                printCommands(allCommandsOrdered);
                System.out.println();
                System.out.println("For spaces use underscores instead. "
                        + "It will be replaced afterwards. E.g. trainer add Prof._Oak");
                System.out.println("Incomplete command followed by question "
                        + "mark will show you list of options. E.g. show ?.");
            } else {

                List<String> commandList = showCommands.get(line);
                if (commandList == null) {
                    System.out.println("Unknown command. Try typing ?.");
                } else {
                    printCommands(commandList);
                }
            }

            return true;
        }

        String tmpLine = line;
        List<String> commandList = null;

        /*
         * throws right most words away so we can get to command root
         * "trainer add [name]" needs only "trainer add" to be recognized
         */
        Boolean shortening = false;
        while (commandList == null && !tmpLine.isEmpty()) {
            commandList = showCommands.get(tmpLine);
            
            /* throw last word away and try again */
            if (commandList == null) {
                int spacePosition = rightIndexOf(tmpLine, ' ');
                tmpLine = tmpLine.substring(0, spacePosition);
                tmpLine = tmpLine.trim();
                shortening = true;
            }
        }
        
        /*
         * Command is still completely unknown.
         *
         * That can also happen if some words already had to be cut to the state
         * that it is now ambiguous (so "trainer asdf" won't result in trainer ...).
         */
        if (commandList == null || commandList.size() > 1 && shortening) {
            System.out.println("Unknown command. Try typing ?.");

        /* Command was found, is it unique? */
        } else {

            /* not unique, error */
            if (commandList.size() > 1) {
                System.out.println("Command is ambiguous. For full command type " + tmpLine + " ?.");

            /*
             * Only one command like this, now test for being in right mode
             * ideally, commands should be already filtered by current mode, but this "detail"
             * would require much bigger effort.
             */
            } else {
                int mode = commandModes.get(tmpLine);
                if (mode != inputMode) {
                    System.out.println(tmpLine + " command is not available in current mode. "
                            + "You need either enter appropriate interface or "
                            + "exit to basic commands.");
                } else {
                    String callback = commandCallbacks.get(tmpLine);

                    Method method = null;
                    try {
                        method = this.getClass().getMethod(callback, String.class);
                    } catch (NoSuchMethodException | SecurityException ex) {
                        Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, ex);
                        System.out.println("Command failed. Please report this incident.");
                    }

                    try {
                        method.invoke(this, line);
                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                        Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, ex);
                        System.out.println("Command failed. Please report this incident.");
                    }
                }
            }

        }
        return true;
    }

    /* 
     * Skip can be grabbed automatically, but would require more effort 
     * to programme that. So number of preceding words before arguments
     * is required to be passed in skip.
     */
    private List<String> getArguments(String command, int skip) {
        StringTokenizer st = new StringTokenizer(command);
        List<String> args = new ArrayList<>();
        
        int i = 0;
        while (st.hasMoreTokens()) {
            i++;

            if (i > skip) {
                String arg = st.nextToken();
                arg = arg.replace("_", " ");

                /* convert to null */
                if (arg.equals("null")) {
                    arg = null;
                }

                args.add(arg);
            } else {
                st.nextToken();
            }
        }

        return args;
    }
    
    public void trainerAdd(String command) {
        List<String> args = getArguments(command, 2);
        
        String name;
        String birth = null;
        String sId = null;

        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }
        
        name = args.get(0);
        
        if (args.size() >= 2) {
            birth = args.get(1);
        }
        
        if (args.size() >= 3) {
            stadiumCity = args.get(2);
        }
        
        /* catch wrong date now */
        if (birth != null) {
            try { 
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date d = df.parse(birth);
            } catch (ParseException ex) {
                System.out.println("Incorrect date format (should be yyyy-mm-dd).");
                return;
            }
        }

        String result = CommandCallback.trainerAdd(name, birth, sId);
        if (result.equals("success")) {
            System.out.println("Trainer created.");
        } else {
            System.out.println(result);
        }
    }

    public void trainerDelete(String command) {
        List<String> args = getArguments(command, 2);
        String id;
        
        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }

        id = args.get(0);

        String result = CommandCallback.generalIdRequest("trainers/" + id, null, "delete");
        if (result.equals("success")) {
            System.out.println("Trainer deleted.");
        } else {
            System.out.println(result);
        }
    }
    
    public void trainerInterface(String command) {
        List<String> args = getArguments(command, 2);
        
        String id;
        
        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }

        id = args.get(0);
        
        try {
            Long.parseLong(id);
        } catch (NumberFormatException ex) {
            System.out.println("Wrong number format.");
            return;
        }

        String result = CommandCallback.generalIdRequest("trainers/" + id, null, "get");
        if (result.substring(0, 7).equals("success")) {
            System.out.println("Trainer loaded.");
            inputMode = 1;
            trainerId = id;
        } else {
            System.out.println(result);
        }

    }
    
    public void showTrainerList(String command) {
        JSONArray jusers = new JSONArray();
        
        String result = CommandCallback.getTrainerList();
        if (result.substring(0, 7).equals("success")) {
            String json_enc = result.substring(8);
            
            try {
                JSONObject json = new JSONObject(json_enc);
                if (json.has("users") && !json.isNull("users")) {
                    jusers = json.getJSONArray("users");
                }

                for (int i = 0; i < jusers.length(); i++) {
                    JSONObject juser = jusers.getJSONObject(i);

                    System.out.println("\tid: " + juser.get("id") + 
                            ", name: " + juser.get("name") + 
                            ", birth: " + juser.get("birth") +
                            ", stadium: " + juser.get("stadium"));
                }
            } catch (JSONException e) {
                System.out.println("Incorrect data from server.");
                Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, e);
            }

        } else {
            System.out.println(result);
        }
    }

    public void stadiumAdd(String command) {
        List<String> args = getArguments(command, 2);
        
        String city;
        String type = null;
        String leader = null;

        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }
        
        city = args.get(0);
        
        if (args.size() >= 2) {
            type = args.get(1);
        }
        
        if (args.size() >= 3) {
            leader = args.get(2);
        }
        
        String result = CommandCallback.stadiumAdd(city, type, leader);
        if (result.equals("success")) {
            System.out.println("Stadium created.");
        } else {
            System.out.println(result);
        }
    }

    public void stadiumDelete(String command) {
        List<String> args = getArguments(command, 2);
        String id;
        
        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }

        id = args.get(0);

        String result = CommandCallback.generalIdRequest("stadiums/" + id, null, "delete");
        if (result.equals("success")) {
            System.out.println("Stadium deleted.");
        } else {
            System.out.println(result);
        }
    }
    
    public void stadiumInterface(String command) {
        List<String> args = getArguments(command, 2);
        
        String city;
        
        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }

        city = args.get(0);

        String result = CommandCallback.generalIdRequest("stadiums/" + city, null, "get");
        if (result.substring(0, 7).equals("success")) {
            System.out.println("Stadium loaded.");
            inputMode = 2;
            stadiumCity = city;
        } else {
            System.out.println(result);
        }

    }
    
    public void showStadiumList(String command) {
        JSONArray jstadiums = new JSONArray();
        
        String result = CommandCallback.getStadiumList();
        if (result.substring(0, 7).equals("success")) {
            String json_enc = result.substring(8);
            
            try {
                JSONObject json = new JSONObject(json_enc);
                if (json.has("stadiums") && !json.isNull("stadiums")) {
                    jstadiums = json.getJSONArray("stadiums");
                }

                for (int i = 0; i < jstadiums.length(); i++) {
                    JSONObject jstadium = jstadiums.getJSONObject(i);

                    System.out.println(
                            "\tcity: " + jstadium.get("city") + 
                            ", type: " + jstadium.get("type") +
                            ", leader: " + jstadium.get("leader"));
                }
            } catch (JSONException e) {
                System.out.println("Incorrect data from server.");
                Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, e);
            }

        } else {
            System.out.println(result);
        }
    }

    public void trainerSetName(String command) {
        List<String> args = getArguments(command, 3);
        
        String name;

        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }
        
        name = args.get(0);
        
        String result = CommandCallback.trainerSetName(trainerId, name);
        if (result.equals("success")) {
            System.out.println("Trainer updated.");
        } else {
            System.out.println(result);
        }
    }

    public void trainerSetBirth(String command) {
        List<String> args = getArguments(command, 3);
        
        String birth;

        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }
        
        birth = args.get(0);
        
        /* catch wrong date now */
        if (birth != null) {
            try { 
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                df.parse(birth);
            } catch (ParseException ex) {
                System.out.println("Incorrect date format (should be yyyy-mm-dd).");
                return;
            }
        }
        
        String result = CommandCallback.trainerSetBirth(trainerId, birth);
        if (result.equals("success")) {
            System.out.println("Trainer updated.");
        } else {
            System.out.println(result);
        }
    }   

    public void trainerSetStadium(String command) {
        List<String> args = getArguments(command, 3);
        
        String stadium;

        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }
        
        stadium = args.get(0);
        
        String result = CommandCallback.trainerSetStadium(trainerId, stadium);
        if (result.equals("success")) {
            System.out.println("Trainer updated.");
        } else {
            System.out.println(result);
        }
    }

    public void showTrainerPokemonList(String command) {
        JSONArray jobjs = new JSONArray();

        String result = CommandCallback.generalIdRequest("pokemons/" + trainerId, null, "get");

        if (result.substring(0, 7).equals("success")) {
            String json_enc = result.substring(8);
            
            try {
                JSONObject json = new JSONObject(json_enc);
                if (json.has("pokemons") && !json.isNull("pokemons")) {
                    jobjs = json.getJSONArray("pokemons");
                }

                for (int i = 0; i < jobjs.length(); i++) {
                    JSONObject jobj = jobjs.getJSONObject(i);

                    System.out.println(
                            "\tname: " + jobj.get("name") + 
                            ", nickname: " + jobj.get("nickname") +
                            ", lvl: " + jobj.get("lvl") +
                            ", type: " + jobj.get("type"));
                }
            } catch (JSONException e) {
                System.out.println("Incorrect data from server.");
                Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, e);
            }

        } else {
            System.out.println(result);
        }
    
    }

    public void showTrainerBadgeList(String command) {
        JSONArray jobjs = new JSONArray();

        String result = CommandCallback.generalIdRequest("badges/" + trainerId, null, "get");

        if (result.substring(0, 7).equals("success")) {
            String json_enc = result.substring(8);
            
            try {
                JSONObject json = new JSONObject(json_enc);
                if (json.has("badges") && !json.isNull("badges")) {
                    jobjs = json.getJSONArray("badges");
                }

                for (int i = 0; i < jobjs.length(); i++) {
                    JSONObject jobj = jobjs.getJSONObject(i);

                    System.out.println(
                            "\tstadium: " + jobj.get("stadium") + 
                            " trainer: " + jobj.get("leader"));
                }
            } catch (JSONException e) {
                System.out.println("Incorrect data from server.");
                Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, e);
            }

        } else {
            System.out.println(result);
        }
    
    }

    public void stadiumSetType(String command) {
        List<String> args = getArguments(command, 3);
        
        String type;

        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }
        
        type = args.get(0);
        
        
        String result = CommandCallback.stadiumSetType(stadiumCity, type);
        if (result.equals("success")) {
            System.out.println("Stadium updated.");
        } else {
            System.out.println(result);
        }
    }   

    public void stadiumSetLeader(String command) {
        List<String> args = getArguments(command, 3);
        
        String leader;

        if (args.size() < 1) {
            System.out.println("Insufficient number of arguments");
            return;
        }
        
        leader = args.get(0);
        
        String result = CommandCallback.stadiumSetLeader(stadiumCity, leader);
        if (result.equals("success")) {
            System.out.println("Stadium updated.");
        } else {
            System.out.println(result);
        }
    }

    public void showStadiumLeader(String command) {

        String result = CommandCallback.getStadiumLeader(stadiumCity);
        if (result.substring(0, 7).equals("success")) {
            String json_enc = result.substring(8);
            
            try {
                JSONObject json = new JSONObject(json_enc);

                System.out.println("\tname: " + json.get("leader"));
            } catch (JSONException e) {
                System.out.println("Incorrect data from server.");
                Logger.getLogger(Commands.class.getName()).log(Level.SEVERE, null, e);
            }

        } else {
            System.out.println(result);
        }
    }

}
