package pa165.fi.muni.pokemon;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // init commands database
        Main obj = new Main();
        Commands commands = new Commands();
        commands.loadCommandsFromFile(obj.getClass().getClassLoader().getResource("commands.dat").getFile());

        System.out.println("Configuration loaded. You can start typing commands. "
                + "For help type ?.");

        // typing commands mode
        Boolean enterCommand = true;
        Scanner scanIn = new Scanner(System.in);
        // loops until "exit" is typed
        while (enterCommand) {
            String line = scanIn.nextLine();
            enterCommand = commands.parseCommand(line);
        }

        scanIn.close();
    }
}
