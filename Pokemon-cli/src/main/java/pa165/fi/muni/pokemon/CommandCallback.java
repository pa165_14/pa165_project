package pa165.fi.muni.pokemon;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author Kuba
 */
public class CommandCallback {
    
    private final static String urlPrefix = "http://localhost:8080/pa165/rest/";

    /*
     * Common function for all requests requiring only id to be set
     */
    public static String generalRequest(String url, String key, String value, String method) {

        List<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair(key, value));

        return Request.makeRequest(urlPrefix + url, nameValuePair, method);
        
    }

    public static String generalIdRequest(String url, String id, String method) {
        return generalRequest(url, "id", id, method);
    }

    public static String trainerAdd(String name, String birth, String stadiumCity) {

        List<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("name", name));
        nameValuePair.add(new BasicNameValuePair("birth", birth));
        nameValuePair.add(new BasicNameValuePair("stadium", stadiumCity));

        return Request.makeRequest(urlPrefix + "trainers", nameValuePair, "post");
    }

    public static String getTrainerList() {
        return Request.makeRequest(urlPrefix + "trainers", null, "get");
    }

    public static String stadiumAdd(String city, String type, String leaderId) {

        List<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("city", city));
        nameValuePair.add(new BasicNameValuePair("type", type));
        nameValuePair.add(new BasicNameValuePair("leader", leaderId));

        return Request.makeRequest(urlPrefix + "stadiums", nameValuePair, "post");
    }
    
    public static String getStadiumList() {
        return Request.makeRequest(urlPrefix + "stadiums", null, "get");
    }

    public static String trainerSetName(String trainerId, String name) {

        List<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("id", trainerId));
        nameValuePair.add(new BasicNameValuePair("name", name));

        return Request.makeRequest(urlPrefix + "trainers", nameValuePair, "put");
    }

    public static String trainerSetBirth(String trainerId, String birth) {

        List<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("id", trainerId));
        nameValuePair.add(new BasicNameValuePair("birth", birth));

        return Request.makeRequest(urlPrefix + "trainers", nameValuePair, "put");
    }

    public static String trainerSetStadium(String trainerId, String stadiumCity) {

        List<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("id", trainerId));
        nameValuePair.add(new BasicNameValuePair("stadium", stadiumCity));

        return Request.makeRequest(urlPrefix + "trainers", nameValuePair, "put");
    }

    public static String stadiumSetType(String stadiumCity, String type) {

        List<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("city", stadiumCity));
        nameValuePair.add(new BasicNameValuePair("type", type));

        return Request.makeRequest(urlPrefix + "stadiums", nameValuePair, "put");
    }

    public static String stadiumSetLeader(String stadiumCity, String trainerId) {

        List<NameValuePair> nameValuePair = new ArrayList<>();
        nameValuePair.add(new BasicNameValuePair("city", stadiumCity));
        nameValuePair.add(new BasicNameValuePair("leader", trainerId));

        return Request.makeRequest(urlPrefix + "stadiums", nameValuePair, "put");
    }

    public static String getStadiumLeader(String stadiumCity) {
        return Request.makeRequest(urlPrefix + "stadiums/" + stadiumCity, null, "get");
    }
}
