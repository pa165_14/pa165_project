package pa165.fi.muni.pokemon.service;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import pa165.fi.muni.pokemon.dao.StadiumDAO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Petr
 */
@Transactional
@Service("stadiumService")
public class StadiumServiceImpl implements StadiumService {

    @Autowired
    private StadiumDAO stadiumDAO;

    @Autowired
    private DozerBeanMapper mapper;

    public StadiumDAO getStadiumDAO() {
        return stadiumDAO;
    }

    public void setStadiumDAO(StadiumDAO stadiumDAO) {
        this.stadiumDAO = stadiumDAO;
    }

    @Override
    public StadiumDTO create(TrainerDTO trainerDTO, String city, Type type) {
        Trainer trainer = mapper.map(trainerDTO, Trainer.class);
        try {
            Stadium stadium = stadiumDAO.create(trainer, city, type);
            return mapper.map(stadium, StadiumDTO.class);
        } catch (Exception ex) {
            throw new DataAccessException("Error when creating stadium", ex) {
            };
        }
    }

    @Override
    public void delete(long id) {
        try {
            stadiumDAO.delete(id);
        } catch (Exception ex) {
            throw new DataAccessException("Error when deleting stadium", ex) {
            };
        }
    }

    @Override
    public void update(StadiumDTO stadiumDTO) {
        Stadium stadium = mapper.map(stadiumDTO, Stadium.class);
        try {
            stadiumDAO.update(stadium);
        } catch (Exception ex) {
            throw new DataAccessException("Error when updating stadium", ex) {
            };
        }
    }

    @Override
    public Boolean assignTrainerToStadium(TrainerDTO trainerDTO, StadiumDTO stadiumDTO) {
        Trainer trainer = mapper.map(trainerDTO, Trainer.class);
        Stadium stadium = mapper.map(stadiumDTO, Stadium.class);
        try {
            return stadiumDAO.assignTrainerToStadium(trainer, stadium);
        } catch (Exception ex) {
            throw new DataAccessException("Error when assigning trainer to stadium", ex) {
            };
        }
    }

    @Override
    public StadiumDTO getStadiumByCity(String city) {
        try {
            Stadium stadium = stadiumDAO.getStadiumByCity(city);
            if (stadium == null) {
                return null;
            }
            return mapper.map(stadium, StadiumDTO.class);
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting stadium by city", ex) {
            };
        }
    }

    @Override
    public List<StadiumDTO> getAllStadiums() {
        try {
            List<Stadium> stadiums = stadiumDAO.getAllStadiums();
            List<StadiumDTO> stadiumsDTO = new ArrayList<>();
            for (Stadium stadium : stadiums) {
                stadiumsDTO.add(mapper.map(stadium, StadiumDTO.class));
            }
            return stadiumsDTO;
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting all stadiums", ex) {
            };
        }
    }

    @Override
    public List<StadiumDTO> getAllStadiumsByType(Type type) {
        try {
            List<Stadium> stadiums = stadiumDAO.getAllStadiumsByType(type);
            List<StadiumDTO> stadiumsDTO = new ArrayList<>();
            for (Stadium stadium : stadiums) {
                stadiumsDTO.add(mapper.map(stadium, StadiumDTO.class));
            }
            return stadiumsDTO;
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting all stadiumsByType", ex) {
            };
        }
    }
}
