package pa165.fi.muni.pokemon.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.dozer.DozerBeanMapper;
import pa165.fi.muni.pokemon.dao.PokemonDAO;
import pa165.fi.muni.pokemon.dto.PokemonDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Pokemon;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

@Transactional
@Service("pokemonService")
public class PokemonServiceImpl implements PokemonService {

    @Autowired
    private PokemonDAO pokemonDAO;

    @Autowired
    private DozerBeanMapper mapper;

    public PokemonDAO getPokemonDAO() {
        return pokemonDAO;
    }

    public void setPokemonDAO(PokemonDAO pokemonDAO) {
        this.pokemonDAO = pokemonDAO;
    }

    @Override
    public PokemonDTO addPokemon(PokemonDTO pokemonDTO) {
        try {
            return create(pokemonDTO.getOwner(), pokemonDTO.getName(), pokemonDTO.getNickname(), pokemonDTO.getType(), pokemonDTO.getLvl());
        } catch (Exception ex) {
            throw new DataAccessException("Error when creating pokemon", ex) {
            };
        }
    }

    @Override
    public PokemonDTO create(TrainerDTO owner, String name, String nickname, Type type, int lvl) {
        try {
            Trainer trainer = mapper.map(owner, Trainer.class);
            Pokemon pokemon = pokemonDAO.create(trainer, name, nickname, type, lvl);

            return mapper.map(pokemon, PokemonDTO.class);

            //return Converter.getDTO(pokemon);
        } catch (Exception ex) {
            throw new DataAccessException("Error when creating pokemon", ex) {
            };
        }
    }

    @Override
    public void delete(long id) {
        try {
            pokemonDAO.delete(id);
        } catch (Exception ex) {
            throw new DataAccessException("Error when deleting pokemon", ex) {
            };
        }
    }

    @Override
    public void update(PokemonDTO pokemonDTO) {
        try {
            Pokemon pokemon = mapper.map(pokemonDTO, Pokemon.class);
            pokemonDAO.update(pokemon);
        } catch (Exception ex) {
            throw new DataAccessException("Error when updating pokemon", ex) {
            };
        }
    }

    @Override
    public PokemonDTO getPokemonById(Long id) {
        try {
            Pokemon pokemon = pokemonDAO.getPokemonById(id);
            return mapper.map(pokemon, PokemonDTO.class);
            //return Converter.getDTO(pokemon);
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting pokemonByID", ex) {
            };
        }
    }

    @Override
    public PokemonDTO getPokemonByNickname(String name) {
        try {
            Pokemon pokemon = pokemonDAO.getPokemonByNickname(name);
            return mapper.map(pokemon, PokemonDTO.class);
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting pokemonByNickname", ex) {
            };
        }
    }

    @Override
    public List<PokemonDTO> getPokemonsOfTrainer(TrainerDTO trainerDTO) {
        try {
            List<Pokemon> pokemons = pokemonDAO.getPokemonsOfTrainer(mapper.map(trainerDTO, Trainer.class));
            List<PokemonDTO> pokemonsDTO = new ArrayList<>();
            for (Pokemon pokemon : pokemons) {
                pokemonsDTO.add(mapper.map(pokemon, PokemonDTO.class));
            }
            return pokemonsDTO;
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting all trainers", ex) {
            };
        }
    }

    @Override
    public List<PokemonDTO> getAllPokemons() {
        try {
            List<Pokemon> pokemons = pokemonDAO.getAllPokemons();
            List<PokemonDTO> pokemonsDTO = new ArrayList<>();
            for (Pokemon pokemon : pokemons) {
                pokemonsDTO.add(mapper.map(pokemon, PokemonDTO.class));
            }
            return pokemonsDTO;
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting all trainers", ex) {
            };
        }
    }
}
