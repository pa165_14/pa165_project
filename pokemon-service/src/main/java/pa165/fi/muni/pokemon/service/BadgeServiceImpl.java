package pa165.fi.muni.pokemon.service;

import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import pa165.fi.muni.pokemon.dao.BadgeDAO;
import pa165.fi.muni.pokemon.dto.BadgeDTO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Badge;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.exception.EntityAlreadyExists;

/**
 *
 * @author Petr
 */
@org.springframework.transaction.annotation.Transactional
@Service("badgeService")
public class BadgeServiceImpl implements BadgeService {

    @Autowired
    private BadgeDAO badgeDAO;

    @Autowired
    private DozerBeanMapper mapper;

    public BadgeDAO getBadgeDAO() {
        return badgeDAO;
    }

    public void setBadgeDAO(BadgeDAO badgeDAO) {
        this.badgeDAO = badgeDAO;
    }

    @Override
    public BadgeDTO create(TrainerDTO trainerDTO, StadiumDTO stadiumDTO) {
        Trainer trainer = mapper.map(trainerDTO, Trainer.class);
        Stadium stadium = mapper.map(stadiumDTO, Stadium.class);

        List<BadgeDTO> badgesDTO = getAllTrainerBadges(trainerDTO);
        for (BadgeDTO badgeDTO : badgesDTO) {
            if (badgeDTO.getStadium().getCity() != null && badgeDTO.getStadium().getCity().equals(stadiumDTO.getCity())) {
                throw new EntityAlreadyExists("Badge already exist.");
            }
        }

        try {
            Badge badge = badgeDAO.create(trainer, stadium);
            return mapper.map(badge, BadgeDTO.class);
        } catch (Exception ex) {
            throw new DataAccessException("Error when creating badge", ex) {
            };
        }
    }

    @Override
    public void delete(long id) {
        try {
            badgeDAO.delete(id);
        } catch (Exception ex) {
            throw new DataAccessException("Error when deleting badge", ex) {
            };
        }
    }

    @Override
    public void update(BadgeDTO badgeDTO) {
        Badge badge = mapper.map(badgeDTO, Badge.class);
        try {
            badgeDAO.update(badge);
        } catch (Exception ex) {
            throw new DataAccessException("Error when updating badge", ex) {
            };
        }
    }

    @Override
    public List<BadgeDTO> getAllTrainerBadges(TrainerDTO holderDTO) {
        try {
            List<Badge> badges = badgeDAO.getAllTrainerBadges(mapper.map(holderDTO, Trainer.class));
            List<BadgeDTO> badgesDTO = new ArrayList<>();
            for (Badge badge : badges) {
                badgesDTO.add(mapper.map(badge, BadgeDTO.class));
            }
            return badgesDTO;
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting all trainer's badges", ex) {
            };
        }
    }

    @Override
    public List<BadgeDTO> getAllBadges() {
        try {
            List<Badge> badges = badgeDAO.getAllBadges();
            List<BadgeDTO> badgesDTO = new ArrayList<>();
            for (Badge badge : badges) {
                badgesDTO.add(mapper.map(badge, BadgeDTO.class));
            }
            return badgesDTO;
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting all badges", ex) {
            };
        }
    }

    @Override
    public int countAllBadges() {
        try {
            return badgeDAO.countAllBadges();
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting badges count", ex) {
            };
        }
    }
}
