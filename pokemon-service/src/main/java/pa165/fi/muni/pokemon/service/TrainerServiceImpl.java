package pa165.fi.muni.pokemon.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import pa165.fi.muni.pokemon.dao.BadgeDAO;
import pa165.fi.muni.pokemon.dao.PokemonDAO;
import pa165.fi.muni.pokemon.dao.StadiumDAO;
import pa165.fi.muni.pokemon.dao.TrainerDAO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Kuba
 */
@Transactional
@Service("trainerService")
public class TrainerServiceImpl implements TrainerService {

    @Autowired
    private TrainerDAO trainerDAO;

    @Autowired
    private StadiumDAO stadiumDAO;

    @Autowired
    private BadgeDAO badgeDAO;

    @Autowired
    private PokemonDAO pokemonDAO;

    @Autowired
    private DozerBeanMapper mapper;

    public TrainerDAO getTrainerDAO() {
        return trainerDAO;
    }

    public void setTrainerDAO(TrainerDAO trainerDAO) {
        this.trainerDAO = trainerDAO;
    }

    public StadiumDAO getStadiumDAO() {
        return stadiumDAO;
    }

    public void setStadiumDAO(StadiumDAO stadiumDAO) {
        this.stadiumDAO = stadiumDAO;
    }

    /*
     * Fill database with data
     */
    @PostConstruct
    public void preloadDB() {
        System.out.println("preloadDB");

        Trainer ash
                = trainerDAO.create("Ash", new GregorianCalendar(1997, 5, 22).getTime());
        Trainer garry
                = trainerDAO.create("Garry", new GregorianCalendar(1997, 9, 29).getTime());
        Trainer saruman
                = trainerDAO.create("Saruman", new GregorianCalendar(-9500, 0, 1).getTime());

        Stadium winterfell
                = stadiumDAO.create(garry, "Winterfell", Type.ICE);
        Stadium isengard
                = stadiumDAO.create(saruman, "Isengard", Type.STEEL);

        badgeDAO.create(ash, winterfell);
        badgeDAO.create(ash, isengard);

        pokemonDAO.create(ash, "Pikachu", "Pikachu", Type.ELECTRIC, 80);
        pokemonDAO.create(ash, "Charizard", "Charmander", Type.FIRE, 46);
        pokemonDAO.create(garry, "Articuno", "Nedd", Type.ICE, 64);
    }

    @Override
    public TrainerDTO create(String name, Date birth) {
        try {
            Trainer trainer = trainerDAO.create(name, birth);
            return mapper.map(trainer, TrainerDTO.class);
        } catch (Exception ex) {
            throw new DataAccessException("Error when creating trainer", ex) {
            };
        }
    }

    @Override
    public void delete(long id) {
        try {
            trainerDAO.delete(id);
        } catch (Exception ex) {
            throw new DataAccessException("Error when deleting trainer", ex) {
            };
        }
    }

    @Override
    public void update(TrainerDTO trainerDTO) {
        try {
            Trainer trainer = mapper.map(trainerDTO, Trainer.class);
            trainerDAO.update(trainer);
        } catch (Exception ex) {
            throw new DataAccessException("Error when updating trainer", ex) {
            };
        }
    }

    @Override
    public TrainerDTO getTrainerById(Long id) {
        try {
            Trainer trainer = trainerDAO.getTrainerById(id);
            if (trainer == null) {
                return null;
            }
            return mapper.map(trainer, TrainerDTO.class);
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting trainerByID", ex) {
            };
        }
    }

    @Override
    public TrainerDTO getTrainerByName(String name) {
        try {
            Trainer trainer = trainerDAO.getTrainerByName(name);
            if (trainer == null) {
                return null;
            }
            return mapper.map(trainer, TrainerDTO.class);
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting trainerByName", ex) {
            };
        }
    }

    @Override
    public List<TrainerDTO> getAllTrainers() {
        try {
            List<Trainer> trainers = trainerDAO.getAllTrainers();
            List<TrainerDTO> trainersDTO = new ArrayList<>();
            for (Trainer trainer : trainers) {
                trainersDTO.add(mapper.map(trainer, TrainerDTO.class));
            }
            return trainersDTO;
        } catch (Exception ex) {
            throw new DataAccessException("Error when getting all trainers", ex) {
            };
        }
    }

    @Override
    public int getCountBadges(TrainerDTO trainerDTO) {
        try {
            Trainer trainer = mapper.map(trainerDTO, Trainer.class);
            return trainerDAO.getCountBadges(trainer);
        } catch (Exception ex) {
            throw new DataAccessException("Error when counting badges", ex) {
            };
        }
    }

    @Override
    public Boolean enterToLeague(TrainerDTO trainerDTO) {
        try {
            Trainer trainer = mapper.map(trainerDTO, Trainer.class);
            return (stadiumDAO.getAllStadiums().size() == trainerDAO.getCountBadges(trainer));
        } catch (Exception ex) {
            throw new DataAccessException("Error when entering to League", ex) {
            };
        }
    }

    @Override
    public Boolean defeat(TrainerDTO trainerDTO, StadiumDTO stadiumDTO) {
        try {
            Trainer trainer = mapper.map(trainerDTO, Trainer.class);
            Stadium stadium = mapper.map(stadiumDTO, Stadium.class);
            // I am trying to defeat myself
            if (trainerDTO.getStadium() != null && trainerDTO.getStadium().getCity().equals(stadium.getCity())) {
                return false;
            }
            // maybe we already defeated this one
            if (trainerDAO.hasBadge(trainer, stadium)) {
                return false;
            }
            badgeDAO.create(trainer, stadium);
            return true;
        } catch (Exception ex) {
            throw new DataAccessException("Error when defeating a leader", ex) {
            };
        }
    }
}
