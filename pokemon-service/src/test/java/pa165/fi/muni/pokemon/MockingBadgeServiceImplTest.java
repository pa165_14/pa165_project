/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon;

import java.util.ArrayList;
import java.util.List;
import org.easymock.EasyMock;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.verify;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pa165.fi.muni.pokemon.dao.BadgeDAO;
import pa165.fi.muni.pokemon.dto.BadgeDTO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;

import pa165.fi.muni.pokemon.entity.Badge;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;
import pa165.fi.muni.pokemon.service.BadgeServiceImpl;
import pa165.fi.muni.pokemon.service.StadiumServiceImpl;
import pa165.fi.muni.pokemon.service.TrainerServiceImpl;

/**
 *
 * @author Kuba
 */
@ContextConfiguration(locations = "/META-INF/persistence-beans-web.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(EasyMockRunner.class)
public class MockingBadgeServiceImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private BadgeServiceImpl badgeService;

    @Autowired
    private StadiumServiceImpl stadiumService;

    @Autowired
    private TrainerServiceImpl trainerService;

    @Autowired
    private org.dozer.Mapper mapper;

    @Mock
    private BadgeDAO badgeDAOMock;

    private Stadium stadium;
    private StadiumDTO stadiumDTO;
    private TrainerDTO holderDTO;
    private Trainer holder;

    @BeforeMethod
    public void before() {
        badgeDAOMock = createStrictMock(BadgeDAO.class);

        holderDTO = trainerService.create("Pryce", null);
        holder = mapper.map(holderDTO, Trainer.class);

        stadiumDTO = stadiumService.create(holderDTO, "Mahogany", Type.ICE);
        stadium = mapper.map(stadiumDTO, Stadium.class);
    }

    @AfterMethod
    public void after() {
        reset(badgeDAOMock);
    }

    @Test
    public void updateTest() {
        badgeService.create(holderDTO, stadiumDTO);

        badgeService.setBadgeDAO(badgeDAOMock);

        Badge badge = new Badge();
        BadgeDTO badgeDTO = new BadgeDTO();

        badgeDAOMock.update(badge);
        EasyMock.replay(badgeDAOMock);
        badgeService.update(badgeDTO);
        EasyMock.verify(badgeDAOMock);
    }

    @Test
    public void createBadgeTest() {

        Badge badge = new Badge();
        badge.setHolder(holder);
        badge.setStadium(stadium);
        BadgeDTO badgeDTO = mapper.map(badge, BadgeDTO.class);

        BadgeDTO realDTO = badgeService.create(holderDTO, stadiumDTO);

        Assert.assertEquals(realDTO, badgeDTO);
    }

    @Test
    public void deleteTest() {
        badgeService.create(holderDTO, stadiumDTO);

        badgeService.setBadgeDAO(badgeDAOMock);

        badgeDAOMock.delete(1);
        EasyMock.replay(badgeDAOMock);
        badgeService.delete(1);
        EasyMock.verify(badgeDAOMock);
    }

    @Test
    public void countAllBadgesTest() {

        Badge badge = new Badge();
        int bdaocount = 3;

        badgeService.setBadgeDAO(badgeDAOMock);

        badgeDAOMock.delete(1);
        EasyMock.replay(badgeDAOMock);
        badgeService.delete(1);
        EasyMock.verify(badgeDAOMock);
    }

    @Test
    public void getAllBadgesTest() {
        badgeService.create(holderDTO, stadiumDTO);

        badgeService.setBadgeDAO(badgeDAOMock);

        List<Badge> badges = new ArrayList<>();

        expect(badgeDAOMock.getAllBadges()).andReturn(badges);
        replay(badgeDAOMock);

        badgeService.getAllBadges();

        verify(badgeDAOMock);
    }
}
