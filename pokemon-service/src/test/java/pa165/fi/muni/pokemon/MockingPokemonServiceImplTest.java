/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon;

import java.util.Calendar;
import org.easymock.EasyMock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;
import pa165.fi.muni.pokemon.service.TrainerService;

import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.verify;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.runner.RunWith;
import org.springframework.dao.DataAccessException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import pa165.fi.muni.pokemon.dao.PokemonDAO;
import pa165.fi.muni.pokemon.dto.PokemonDTO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Pokemon;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;
import pa165.fi.muni.pokemon.service.PokemonServiceImpl;

/**
 *
 * @author Petr
 */
@ContextConfiguration(locations = "/META-INF/persistence-beans-web.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(EasyMockRunner.class)
public class MockingPokemonServiceImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private TrainerService trainerService;

    @Autowired
    private PokemonServiceImpl pokemonService;

    @Autowired
    private org.dozer.Mapper mapper;

    @Mock
    private PokemonDAO pokemonDAOMock;

    private Stadium stadium;
    private StadiumDTO stadiumDTO;
    private TrainerDTO trainer1DTO;
    private TrainerDTO trainer2DTO;
    private Trainer trainer1;
    private Trainer trainer2;
    private Pokemon pokemon;
    private PokemonDTO pokemonDTO;

    private final int lvl = 10;

    @BeforeMethod
    public void before() {
        pokemonDAOMock = createStrictMock(PokemonDAO.class);

        Calendar cal = Calendar.getInstance();
        cal.set(1960, 2, 8);

        trainer1DTO = new TrainerDTO("Garry", cal.getTime(), null);
        trainer1 = mapper.map(trainer1DTO, Trainer.class);

        pokemonDTO = new PokemonDTO("Charizard", Type.FIRE, trainer1DTO);
        pokemonDTO.setLvl(lvl);
        pokemon = mapper.map(pokemonDTO, Pokemon.class);

        trainer2DTO = new TrainerDTO("Pryce", cal.getTime(), null);
        trainer2 = mapper.map(trainer2DTO, Trainer.class);

        stadiumDTO = new StadiumDTO(1, "Mahogany", Type.ICE, null);
        stadium = mapper.map(stadiumDTO, Stadium.class);
    }

    @AfterMethod
    public void after() {
        reset(pokemonDAOMock);
    }

    @Test
    public void createPokemonTest() {

        /* Inject the mock dependency */
        pokemonService.setPokemonDAO(pokemonDAOMock);

        /* Set the expectations */
        expect(pokemonDAOMock.create(pokemon.getOwner(), pokemon.getName(), pokemon.getNickname(), pokemon.getType(), pokemon.getLvl())).andReturn(pokemon);
        replay(pokemonDAOMock);

        /* Run the test */
        pokemonService.create(mapper.map(pokemonDTO.getOwner(), TrainerDTO.class), pokemonDTO.getName(), pokemonDTO.getNickname(), pokemonDTO.getType(), pokemonDTO.getLvl());

        /* Verify that the mock's expectations were met */
        verify(pokemonDAOMock);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void addPokemonWithNullTest() {
        pokemonService.addPokemon(null);
    }

    @Test
    public void updateTest() {
        pokemonService.setPokemonDAO(pokemonDAOMock);

        Pokemon pokemon1 = new Pokemon();
        PokemonDTO pokemonDTO1 = new PokemonDTO();

        pokemonDAOMock.update(pokemon1);
        EasyMock.replay(pokemonDAOMock);
        pokemonService.update(pokemonDTO1);
        EasyMock.verify(pokemonDAOMock);
    }

    @Test
    public void deleteTest() {
        pokemonService.setPokemonDAO(pokemonDAOMock);

        pokemonDAOMock.delete(1);
        EasyMock.replay(pokemonDAOMock);
        pokemonService.delete(1);
        EasyMock.verify(pokemonDAOMock);
    }

    @Test
    public void getPokemonByIdTest() {

        /* Inject the mock dependency */
        pokemonService.setPokemonDAO(pokemonDAOMock);

        /* Set the expectations */
        expect(pokemonDAOMock.getPokemonById(pokemon.getId())).andReturn(pokemon);
        replay(pokemonDAOMock);

        /* Run the test */
        pokemonService.getPokemonById(pokemonDTO.getId());

        /* Verify that the mock's expectations were met */
        verify(pokemonDAOMock);
    }

    @Test
    public void getPokemonByNicknameTest() {

        /* Inject the mock dependency */
        pokemonService.setPokemonDAO(pokemonDAOMock);

        /* Set the expectations */
        expect(pokemonDAOMock.getPokemonByNickname(pokemon.getNickname())).andReturn(pokemon);
        replay(pokemonDAOMock);

        /* Run the test */
        pokemonService.getPokemonByNickname(pokemonDTO.getNickname());

        /* Verify that the mock's expectations were met */
        verify(pokemonDAOMock);
    }
}
