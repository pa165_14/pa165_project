/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.verify;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;

import org.junit.runner.RunWith;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pa165.fi.muni.pokemon.dto.BadgeDTO;
import pa165.fi.muni.pokemon.dao.TrainerDAO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Badge;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;
import pa165.fi.muni.pokemon.service.TrainerServiceImpl;

/**
 *
 * @author Petr
 */
@ContextConfiguration(locations = "/META-INF/persistence-beans-web.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(EasyMockRunner.class)
public class MockingTrainerServiceImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private TrainerServiceImpl trainerService;

    @Autowired
    private org.dozer.Mapper mapper;

    @Mock
    private TrainerDAO trainerDAOMock;

    private Stadium stadium;
    private StadiumDTO stadiumDTO;
    private TrainerDTO trainer1DTO;
    private TrainerDTO trainer2DTO;
    private Trainer trainer1;
    private Trainer trainer2;
    private BadgeDTO badgeDTO;
    private Badge badge;

    @BeforeMethod
    public void before() {
        trainerDAOMock = createStrictMock(TrainerDAO.class);

        Calendar cal = Calendar.getInstance();
        cal.set(1960, 2, 8);

        trainer1DTO = new TrainerDTO("Garry", cal.getTime(), null);
        trainer1 = mapper.map(trainer1DTO, Trainer.class);

        trainer2DTO = new TrainerDTO("Pryce", cal.getTime(), null);
        trainer2 = mapper.map(trainer2DTO, Trainer.class);

        stadiumDTO = new StadiumDTO(1, "Mahogany", Type.ICE, null);
        stadium = mapper.map(stadiumDTO, Stadium.class);

        badgeDTO = new BadgeDTO(1, trainer1DTO, stadiumDTO);
        badge = mapper.map(badgeDTO, Badge.class);

    }

    @AfterMethod
    public void after() {
        reset(trainerDAOMock);
    }

    @Test
    public void createTrainerTest() {

        /* Inject the mock dependency */
        trainerService.setTrainerDAO(trainerDAOMock);

        /* Set the expectations */
        expect(trainerDAOMock.create(trainer1.getName(), trainer1.getBirth())).andReturn(trainer1);
        replay(trainerDAOMock);

        /* Run the test */
        TrainerDTO realDTO = trainerService.create(trainer1DTO.getName(), trainer1DTO.getBirth());

        /* Verify that the mock's expectations were met */
        verify(trainerDAOMock);
        Assert.assertEquals(realDTO, trainer1DTO);
    }

    @Test
    public void getTrainerByIdTest() {

        /* Inject the mock dependency */
        trainerService.setTrainerDAO(trainerDAOMock);

        /* Set the expectations */
        expect(trainerDAOMock.getTrainerById(trainer1.getId())).andReturn(trainer1);
        replay(trainerDAOMock);

        /* Run the test */
        TrainerDTO realDTO = trainerService.getTrainerById(trainer1DTO.getId());

        /* Verify that the mock's expectations were met */
        verify(trainerDAOMock);
        Assert.assertEquals(realDTO, trainer1DTO);

    }
//    

    @Test
    public void getTrainerByNameTest() {
        /* Inject the mock dependency */
        trainerService.setTrainerDAO(trainerDAOMock);

        /* Set the expectations */
        expect(trainerDAOMock.getTrainerByName(trainer1.getName())).andReturn(trainer1);
        replay(trainerDAOMock);

        /* Run the test */
        TrainerDTO realDTO = trainerService.getTrainerByName(trainer1DTO.getName());

        /* Verify that the mock's expectations were met */
        verify(trainerDAOMock);
        Assert.assertEquals(realDTO, trainer1DTO);

    }

    @Test
    public void getCountBadgesTest() {
        /* Inject the mock dependency */
        trainerService.setTrainerDAO(trainerDAOMock);
        expect(trainerDAOMock.create(trainer1.getName(), trainer1.getBirth())).andReturn(trainer1);
        expect(trainerDAOMock.create(trainer2.getName(), trainer2.getBirth())).andReturn(trainer2);
        expect(trainerDAOMock.getCountBadges(trainer1)).andReturn(2);
        replay(trainerDAOMock);

        /* Run the test */
        trainerService.create(trainer1DTO.getName(), trainer1DTO.getBirth());
        trainerService.create(trainer2DTO.getName(), trainer2DTO.getBirth());
        int realCount = trainerService.getCountBadges(trainer1DTO);

        /* Verify that the mock's expectations were met */
        verify(trainerDAOMock);
    }

    @Test
    public void getAllTrainersTest() {

        /* Inject the mock dependency */
        trainerService.setTrainerDAO(trainerDAOMock);

        List<Trainer> trainersList = new ArrayList<>();
        trainersList.add(trainer1);
        trainersList.add(trainer2);

        expect(trainerDAOMock.create(trainer1.getName(), trainer1.getBirth())).andReturn(trainer1);
        expect(trainerDAOMock.create(trainer2.getName(), trainer2.getBirth())).andReturn(trainer2);
        expect(trainerDAOMock.getAllTrainers()).andReturn(trainersList);
        replay(trainerDAOMock);

        /* Run the test */
        trainerService.create(trainer1DTO.getName(), trainer1DTO.getBirth());
        trainerService.create(trainer2DTO.getName(), trainer2DTO.getBirth());
        List<TrainerDTO> trainersDTO = trainerService.getAllTrainers();

        /* Verify that the mock's expectations were met */
        verify(trainerDAOMock);
        Assert.assertEquals(trainersDTO.size(), trainersList.size());
    }

    @Test
    public void enterToLeague() {
        trainerService.setTrainerDAO(trainerDAOMock);

        expect(trainerDAOMock.getCountBadges(trainer1)).andReturn(1);
        replay(trainerDAOMock);
        trainerService.enterToLeague(trainer1DTO);
        verify(trainerDAOMock);
    }

    @Test
    public void defeat() {
        trainerService.setTrainerDAO(trainerDAOMock);

        expect(trainerDAOMock.hasBadge(trainer2, stadium)).andReturn(true);
        replay(trainerDAOMock);
        trainerService.defeat(trainer1DTO, stadiumDTO);
        verify(trainerDAOMock);
    }
}
