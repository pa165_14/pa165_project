/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.verify;
import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pa165.fi.muni.pokemon.dao.StadiumDAO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.entity.Stadium;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Trainer;
import pa165.fi.muni.pokemon.entity.Type;
import pa165.fi.muni.pokemon.service.StadiumServiceImpl;

/**
 *
 * @author gabrieltoth
 */
@ContextConfiguration(locations = "/META-INF/persistence-beans-web.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@RunWith(EasyMockRunner.class)
public class MockingStadiumServiceImplTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private StadiumServiceImpl stadiumService;

    @Autowired
    private org.dozer.Mapper mapper;

    @Mock
    private StadiumDAO stadiumDAOMock;

    private Stadium stadium;
    private StadiumDTO stadiumDTO;
    private TrainerDTO trainer1DTO;
    private Trainer trainer1;

    @BeforeMethod
    public void before() {
        stadiumDAOMock = createStrictMock(StadiumDAO.class);

        Calendar cal = Calendar.getInstance();
        cal.set(1960, 2, 8);

        trainer1DTO = new TrainerDTO("Garry", cal.getTime(), null);
        trainer1 = mapper.map(trainer1DTO, Trainer.class);

        stadiumDTO = new StadiumDTO(1, "Mahogany", Type.ICE, trainer1DTO);
        stadium = mapper.map(stadiumDTO, Stadium.class);
    }

    @AfterMethod
    public void after() {
        reset(stadiumDAOMock);
    }

    @Test
    public void createStadiumTest() {

        /* Inject the mock dependency */
        stadiumService.setStadiumDAO(stadiumDAOMock);

        Calendar cal = Calendar.getInstance();
        cal.set(1997, 5, 22);

        /* Set the expectations */
        expect(stadiumDAOMock.create(stadium.getLeader(), stadium.getCity(), stadium.getType())).andReturn(stadium);

        replay(stadiumDAOMock);

        /* Run the test */
        StadiumDTO realDTO = stadiumService.create(
                mapper.map(stadiumDTO.getLeader(), TrainerDTO.class),
                stadiumDTO.getCity(), stadiumDTO.getType());

        /* Verify that the mock's expectations were met */
        verify(stadiumDAOMock);
        Assert.assertEquals(realDTO, stadiumDTO);
    }

    @Test
    public void deleteStadiumTest() {
        stadiumService.setStadiumDAO(stadiumDAOMock);

        stadiumDAOMock.delete(1);
        replay(stadiumDAOMock);
        stadiumService.delete(1);
        verify(stadiumDAOMock);
    }

    @Test
    public void updateStadiumTest() {
        stadiumService.setStadiumDAO(stadiumDAOMock);

        stadiumDAOMock.update(stadium);
        replay(stadiumDAOMock);
        stadiumService.update(stadiumDTO);
        verify(stadiumDAOMock);
    }

    @Test
    public void assignTrainerToStadiumTest() {
        stadiumService.setStadiumDAO(stadiumDAOMock);

        Calendar cal = Calendar.getInstance();
        cal.set(1997, 5, 22);

        Stadium stadium2 = new Stadium();
        stadium2.setCity("Konoha");
        stadium2.setId(20);
        stadium2.setType(Type.FIRE);

        Trainer trainer = new Trainer();
        trainer.setId(2);
        trainer.setName("Garry");
        trainer.setBirth(cal.getTime());
        TrainerDTO trainer0DTO = mapper.map(trainer, TrainerDTO.class);

        Trainer trainer2 = stadium2.getLeader();
        TrainerDTO trainer2DTO = null;
        if (trainer2 != null) {
            mapper.map(trainer2, TrainerDTO.class);
        }

        StadiumDTO stadium2DTO = new StadiumDTO(stadium2.getId(),
                stadium2.getCity(), stadium2.getType(), trainer2DTO);
        stadium2DTO.setId(20);

        TrainerDTO trainerDTO = new TrainerDTO(trainer.getName(),
                trainer.getBirth(), trainer0DTO.getStadium());
        trainerDTO.setId(2);

        expect(stadiumDAOMock.assignTrainerToStadium(trainer, stadium2))
                .andReturn(Boolean.TRUE);
        replay(stadiumDAOMock);

        stadiumService.assignTrainerToStadium(trainerDTO, stadium2DTO);

        verify(stadiumDAOMock);
        Stadium newStadium = mapper.map(stadium2DTO, Stadium.class);
        Assert.assertEquals(stadium2, newStadium);
    }

    @Test
    public void getStadiumByCityTest() {
        stadiumService.setStadiumDAO(stadiumDAOMock);

        String city = "Konoha";

        Stadium stadium2 = new Stadium();
        stadium2.setCity(city);

        expect(stadiumDAOMock.getStadiumByCity(city)).andReturn(stadium2);
        replay(stadiumDAOMock);
        StadiumDTO newStadium = stadiumService.getStadiumByCity(city);
        verify(stadiumDAOMock);
        Assert.assertEquals(stadium2, mapper.map(newStadium, Stadium.class));
    }

    @Test
    public void getAllStadiumsTest() {
        stadiumService.setStadiumDAO(stadiumDAOMock);

        List<Stadium> stadiumList = new ArrayList<>();
        String city = "Konoha";
        String city2 = "Winterfell";

        Stadium stadium1 = new Stadium();
        stadium1.setCity(city);

        Stadium stadium2 = new Stadium();
        stadium2.setCity(city2);

        stadiumList.add(stadium1);
        stadiumList.add(stadium2);

        expect(stadiumDAOMock.getAllStadiums()).andReturn(stadiumList);
        replay(stadiumDAOMock);

        List<StadiumDTO> actualList = stadiumService.getAllStadiums();

        verify(stadiumDAOMock);
        List<Stadium> newList = new ArrayList<>();
        for (StadiumDTO dto : actualList) {
            newList.add(mapper.map(dto, Stadium.class));
        }
        Assert.assertEquals(stadiumList, newList);
    }

    @Test
    public void getAllStadiumsByTypeTest() {
        stadiumService.setStadiumDAO(stadiumDAOMock);

        List<Stadium> stadiumList = new ArrayList<>();
        String city = "Konoha";
        String city2 = "Winterfell";
        Type type = Type.ICE;

        Stadium stadium1 = new Stadium();
        stadium1.setCity(city);
        stadium1.setType(type);

        Stadium stadium2 = new Stadium();
        stadium2.setCity(city2);
        stadium2.setType(type);

        stadiumList.add(stadium1);
        stadiumList.add(stadium2);

        expect(stadiumDAOMock.getAllStadiumsByType(type)).andReturn(stadiumList);
        replay(stadiumDAOMock);

        List<StadiumDTO> actualList = stadiumService.getAllStadiumsByType(type);

        verify(stadiumDAOMock);
        List<Stadium> newList = new ArrayList<>();
        for (StadiumDTO dto : actualList) {
            newList.add(mapper.map(dto, Stadium.class));
        }
        Assert.assertEquals(stadiumList, newList);
    }
}
