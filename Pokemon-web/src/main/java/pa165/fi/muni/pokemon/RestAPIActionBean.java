/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pa165.fi.muni.pokemon.dto.BadgeDTO;
import pa165.fi.muni.pokemon.dto.PokemonDTO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Type;
import pa165.fi.muni.pokemon.service.BadgeService;
import pa165.fi.muni.pokemon.service.PokemonService;
import pa165.fi.muni.pokemon.service.StadiumService;
import pa165.fi.muni.pokemon.service.TrainerService;
import pa165.fi.muni.pokemon.util.JSONResolution;

/**
 *
 * @author Gabriel
 */
@UrlBinding("/rest/{$event}/{id = -1}")
public class RestAPIActionBean extends BaseActionBean implements ValidationErrorHandler {

    private String id;

    final static Logger log = LoggerFactory.getLogger(TrainersActionBean.class);

    @SpringBean //Spring can inject even to private and protected fields
    protected TrainerService trainerService;

    @SpringBean //Spring can inject even to private and protected fields
    protected StadiumService stadiumService;

    @SpringBean //Spring can inject even to private and protected fields
    protected BadgeService badgeService;

    @SpringBean //Spring can inject even to private and protected fields
    protected PokemonService pokemonService;

    //--- part for showing a list of trainers ----
    private List<TrainerDTO> trainers;
    private List<BadgeDTO> badges;
    private List<PokemonDTO> pokemons;
    private List<StadiumDTO> stadiums;

    public List<TrainerDTO> getTrainers() {
        return trainers;
    }

    public List<BadgeDTO> getBadges() {
        return badges;
    }

    public List<PokemonDTO> getPokemons() {
        return pokemons;
    }

    public List<StadiumDTO> getStadiums() {
        return stadiums;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    //--- part for adding a trainer ----
    @ValidateNestedProperties(value = {
        @Validate(on = {"add", "save"}, field = "name", required = true),})
    private TrainerDTO trainer;

    public TrainerDTO getTrainer() {
        return trainer;
    }

    public void setTrainer(TrainerDTO trainer) {
        this.trainer = trainer;
    }
    
    private Boolean isAuthenticated() {
        String authString = "rest" + ":" + "rest";

        String encoded = "";
        String authHeader = getContext().getRequest().getHeader("Authorization");
        
        try {
            encoded = Base64.getEncoder().encodeToString(authString.getBytes("utf-8"));
        } catch (UnsupportedEncodingException ex) {
        }
        
        return encoded.equals(authHeader);
    }

    @DefaultHandler
    @HandlesEvent("all")
    public Resolution list() {
        String method = getContext().getRequest().getMethod();
        String jo = "Welcome to API. Use /trainers or /stadiums please";

        return new JSONResolution(jo);
    }

    //TRAINER HANDLER AND METHODS
    @HandlesEvent("trainers")
    public Resolution listT() throws IOException {
        String method = getContext().getRequest().getMethod();
        String auth = getContext().getRequest().getHeader("authorization");
        String jo = "";
        String ret = "";
        
        if (!isAuthenticated()) {
            return new JSONResolution("Not authenticated.");
        }

        if (method.equals("GET")) {
            try {
                if (Long.parseLong(id) <= 0) {
                    jo = returnTrainerList();
                } else {
                    jo = returnTrainer(Long.parseLong(id));
                    if (jo == null) {
                        ret = "Trainer not found.";
                    }

                }
            } catch (NumberFormatException ex) {
                jo = null;
                ret = "Wrong request.";
            }

            if (jo != null) {
                ret = "success: " + jo;
            }
        }

        if (method.equals("POST")) {
            String contenttype = "";
            String name = "";
            String birthDate = "";
            String stadiumCity = "";
            BufferedReader reader = getContext().getRequest().getReader();
            contenttype = getContext().getRequest().getHeader("Content-Type");           
            Map<String, String> parameters = new LinkedHashMap<String,String> ();
            if (!contenttype.equals("application/json")){
                name = getContext().getRequest().getParameter("name");
                birthDate = getContext().getRequest().getParameter("birth");
                stadiumCity = getContext().getRequest().getParameter("stadium");
            }
            else {
                JsonReader jreader = Json.createReader(reader);
                JsonObject obj = jreader.readObject();
                jreader.close();
                String tName = obj.get("name").toString();
                String tBirth = obj.get("birth").toString();

                int endIndex = tName.length() - 1;
                name = tName.substring(1,endIndex);
                endIndex = tBirth.length() - 1;
                birthDate = tBirth.substring(1,endIndex);
                if(obj.get("stadium") != null){
                    String tStadium = obj.get("stadium").toString();
                    endIndex = tStadium.length() - 1;
                    stadiumCity = tStadium.substring(1,endIndex);
                }

                
            }
            reader.close();

            ret = createNewTrainer(name, birthDate, stadiumCity);
        }

        if (method.equals("DELETE")) {
            try {
                String str = deleteTrainer(Long.parseLong(id));
                if ("success".equals(str)) {
                    ret = "success";
                } else {
                    ret = str;
                }
            } catch (NumberFormatException ex) {
                ret = "Wrong request.";
            }
        }
        String contenttype = "";
        if (method.equals("PUT")) {

            BufferedReader reader = getContext().getRequest().getReader();
            contenttype = getContext().getRequest().getHeader("Content-Type");
            Map<String, String> parameters = new LinkedHashMap<String, String>();
            if (!contenttype.equals("application/json")) {
                String line = reader.readLine();
                parameters = splitQuery(line);
            } else {
                JsonReader jreader = Json.createReader(reader);
                JsonObject obj = jreader.readObject();
                jreader.close();
                for (String key : obj.keySet()) {
                    String value = obj.get(key).toString();
                    int endIndex = value.length() - 1;
                    parameters.put(key, value.substring(1, endIndex));
                }
            }
            reader.close();
            String name = parameters.get("name");
            String birthDate = parameters.get("birth");
            String stadiumCity = parameters.get("stadium");
            String trainerId = parameters.get("id");

            if (name != null) {
                ret = updateTrainerName(trainerId, name);
            }

            if (birthDate != null) {
                ret = updateTrainerBirth(trainerId, birthDate);
            }

            if (stadiumCity != null) {
                ret = updateTrainerStadium(trainerId, stadiumCity);
            }

        }

        return new JSONResolution(ret);
    }

    //GET without ID or ID=0 (default value)
    public String returnTrainerList() {
        JsonArrayBuilder ja = Json.createArrayBuilder();
        JsonObjectBuilder jb = Json.createObjectBuilder();
        for (TrainerDTO trainerDto : trainerService.getAllTrainers()) {
            String stadium = "";
            if (trainerDto.getStadium() != null) {
                stadium = trainerDto.getStadium().getCity();
            }

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String bDate = df.format(trainerDto.getBirth());

            ja.add(Json.createObjectBuilder()
                    .add("id", trainerDto.getId())
                    .add("name", trainerDto.getName())
                    .add("birth", bDate)
                    .add("stadium", stadium));
        }
        jb.add("users", ja.build());
        JsonObject jo = jb.build();
        return jo.toString();
    }

    //GET trainer with specific ID
    public String returnTrainer(Long id) {
        TrainerDTO trainerDto = trainerService.getTrainerById(id);
        String stadium = "";

        if (trainerDto == null) {
            return null;
        }

        if (trainerDto.getStadium() != null) {
            stadium = trainerDto.getStadium().getCity();
        }

        JsonObject jo = Json.createObjectBuilder()
                .add("id", trainerDto.getId())
                .add("name", trainerDto.getName())
                .add("birth", trainerDto.getBirth().toString())
                .add("stadium", stadium)
                .build();
        return jo.toString();
    }

    public String createNewTrainer(String name, String birth, String stadiumCity) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date b = null;

        if (birth != null && !birth.isEmpty()) {
            try {
                b = df.parse(birth);
            } catch (ParseException ex) {
                return "Invalid date format";
            }
        }
        
        if (trainerService.getTrainerByName(name) != null)
        {
            return "This trainer already exists.";
        }
        
        TrainerDTO trainerDto = trainerService.create(name, b);

        if (stadiumCity != null && !stadiumCity.isEmpty()) {
            StadiumDTO stadiumDto = stadiumService.getStadiumByCity(stadiumCity);

            if (stadiumDto != null) {
                trainerDto.setStadium(stadiumDto);
                trainerService.update(trainerDto);
            } else {
                return "Trainer created but stadium was not found.";
            }
        } else if (stadiumCity != null && stadiumCity.isEmpty()) {
            return "success";
        } else {
            return "Trainer created but stadium doesn't exist.";
        }
        return "success";
    }

    public String updateTrainerName(String trainerId, String name) {
        long tId = Long.parseLong(trainerId);

        TrainerDTO trainerDto = trainerService.getTrainerById(tId);

        if (trainerDto == null) {
            return "Trainer not found.";
        }

        trainerDto.setName(name);
        trainerService.update(trainerDto);

        return "success";
    }

    public String updateTrainerBirth(String trainerId, String strBirth) {
        long tId = Long.parseLong(trainerId);

        TrainerDTO trainerDto = trainerService.getTrainerById(tId);

        if (trainerDto == null) {
            return "Trainer not found.";
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dateBirth;
        try {
            dateBirth = df.parse(strBirth);
        } catch (ParseException ex) {
            return "Invalid date format.";
        }

        trainerDto.setBirth(dateBirth);
        trainerService.update(trainerDto);

        return "success";
    }

    public String updateTrainerStadium(String trainerId, String stadiumCity) {
        long tId = Long.parseLong(trainerId);

        TrainerDTO trainerDto = trainerService.getTrainerById(tId);
        StadiumDTO stadiumDto = stadiumService.getStadiumByCity(stadiumCity);

        if (trainerDto == null) {
            return "Trainer not found.";
        }

        if (stadiumDto == null) {
            return "Stadium not found.";
        }
        if (stadiumDto.getLeader() != null) {
            return "Stadium already has its leader.";
        }
        
        trainerDto.setStadium(stadiumDto);
        trainerService.update(trainerDto);

        return "success";
    }

    public String deleteTrainer(Long trainerId) {
        trainer = trainerService.getTrainerById(trainerId);

        if (trainer == null) {
            return "Trainer don't exist";
        } else if (trainer.getStadium() != null) {
            return "Trainer is stadium leader, please delete stadium first.";
        }
        trainerService.delete(trainerId);
        return "success";
    }

    //STADIUM HANDLER AND METHODS
    @HandlesEvent("stadiums")
    public Resolution stadiums() throws IOException {
        String method = getContext().getRequest().getMethod();
        String jo = "";
        String ret = "";

        if (!isAuthenticated()) {
            return new JSONResolution("Not authenticated.");
        }

        System.out.println("id is: " + id);
        if (method.equals("GET")) {
            try {
                Long stadiumID = Long.parseLong(id);
                if (stadiumID >=0) {
                    throw new NumberFormatException();
                }
                jo = returnStadiumList();
            } catch (NumberFormatException ex) {
                jo = returnStadium(id);     // finds stadium by city
                if (jo == null) {
                    ret = "Stadium not found.";
                }
            }
            if (jo != null) {
                ret = "success: " + jo;
            }

        }

        if (method.equals("POST")) {
            String contenttype = "";
            String city = "";
            String type = "";
            String leaderId = "";
            BufferedReader reader = getContext().getRequest().getReader();
            contenttype = getContext().getRequest().getHeader("Content-Type");           
            Map<String, String> parameters = new LinkedHashMap<String,String> ();
            if (!contenttype.equals("application/json")){
                city = getContext().getRequest().getParameter("city");
                type = getContext().getRequest().getParameter("type");
                leaderId = getContext().getRequest().getParameter("leader");
            }
            else {
                JsonReader jreader = Json.createReader(reader);
                JsonObject obj = jreader.readObject();
                jreader.close();
                String tCity = obj.get("city").toString();
                String tType = obj.get("type").toString();
                String tLeader = obj.get("leader").toString();
                
                int endIndex = tCity.length() - 1;
                city = tCity.substring(1,endIndex);
                endIndex = tType.length() - 1;
                type = tType.substring(1,endIndex);
                endIndex = tLeader.length() - 1;
                leaderId = tLeader.substring(1,endIndex);
                
            }
            reader.close();

            ret = createNewStadium(city, type, leaderId);
        }

        if (method.equals("DELETE")) {
            try {
                String str = deleteStadium(id);
                if ("success".equals(str)) {
                    ret = "success";
                } else {
                    ret = str;
                }
            } catch (NumberFormatException ex) {
                ret = "Wrong request.";
            }
        }
        
        if (method.equals("PUT")) {
            String contenttype = "";
            BufferedReader reader = getContext().getRequest().getReader();
            contenttype = getContext().getRequest().getHeader("Content-Type");           
            Map<String, String> parameters = new LinkedHashMap<String,String> ();
            if (!contenttype.equals("application/json")){
                String line = reader.readLine();
                parameters = splitQuery(line);
            }
            else {
                JsonReader jreader = Json.createReader(reader);
                JsonObject obj = jreader.readObject();
                jreader.close();
                for (String key : obj.keySet()){
                    String value = obj.get(key).toString();
                    int endIndex = value.length() - 1;
                    parameters.put(key, value.substring(1, endIndex));
                }
            }
            reader.close();

            String stadiumCity = parameters.get("city");
            String type = parameters.get("type");
            String leaderId = parameters.get("leader");

            if (type != null) {
                ret = updateStadiumType(stadiumCity, type);
            }

            if (leaderId != null) {
                ret = updateStadiumLeader(stadiumCity, leaderId);
            }

        }

        return new JSONResolution(ret);
    }

    public String returnStadiumList() {
        JsonArrayBuilder ja = Json.createArrayBuilder();
        JsonObjectBuilder jb = Json.createObjectBuilder();
        for (StadiumDTO stadiumDto : stadiumService.getAllStadiums()) {
            String leader = "";
            if (stadiumDto.getLeader() != null) {
                leader = stadiumDto.getLeader().getName();
            }
            ja.add(Json.createObjectBuilder()
                    .add("id", stadiumDto.getId())
                    .add("city", stadiumDto.getCity())
                    .add("type", stadiumDto.getType().toString())
                    .add("leader", leader));
        }
        jb.add("stadiums", ja.build());
        JsonObject jo = jb.build();
        return jo.toString();
    }

    //GET trainer with specific ID
    public String returnStadium(String city) {
        System.out.println("city: " + city);
        
        StadiumDTO stadiumDto = stadiumService.getStadiumByCity(city);
        String leader = "";

        if (stadiumDto == null) {
            return null;
        }

        if (stadiumDto.getLeader() != null) {
            leader = stadiumDto.getLeader().getName();
        }

        JsonObject jo = Json.createObjectBuilder()
                .add("city", stadiumDto.getCity())
                .add("type", stadiumDto.getType().toString())
                .add("leader", leader)
                .build();
        return jo.toString();
    }

    public String createNewStadium(String city, String type, String leaderId) {
        try {
            trainer = trainerService.getTrainerById(Long.parseLong(leaderId));
        } catch (NumberFormatException ex) {
            return "Invalid trainerID argument.";
        }

        if (trainer == null) {
            return "Trainer doesn't exist. Stadium wasn't created.";
        } else if (trainer.getStadium() != null) {
            return "Trainer is already leader. Stadium wasn't created.";
        }

        Type typ;
        try {
            typ = Type.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException ex) {
            return "Not valid type.";
        }
        if (stadiumService.getStadiumByCity(city) != null) {
            return "Stadium in this city already exists.";
        }

        TrainerDTO trainerDto = trainerService.getTrainerById(Long.parseLong(leaderId));
        stadiumService.create(trainerDto, city, typ);

        return "success";
    }

    public String updateStadiumType(String stadiumCity, String type) {
        StadiumDTO stadiumDto = stadiumService.getStadiumByCity(stadiumCity);

        if (stadiumDto == null) {
            return "Stadium not found.";
        }

        Type typ;
        try {
            typ = Type.valueOf(type.toUpperCase());
        } catch (java.lang.IllegalArgumentException ex) {
            return "Not valid type.";
        }

        stadiumDto.setType(typ);
        stadiumService.update(stadiumDto);

        return "success";
    }

    public String updateStadiumLeader(String stadiumCity, String trainerId) {
        long tId;
        try {
            tId = Long.parseLong(trainerId);
        } catch (NumberFormatException ex) {
            return "Leader has to be set by ID.";
        }
       
        TrainerDTO trainerDto = trainerService.getTrainerById(tId);
        StadiumDTO stadiumDto = stadiumService.getStadiumByCity(stadiumCity);

        if (trainerDto == null) {
            return "Trainer not found.";
        }

        if (stadiumDto == null) {
            return "Stadium not found.";
        }

        stadiumDto.setLeader(trainerDto);
        try {
            stadiumService.update(stadiumDto);
        } catch (Exception e) {
            return "Can't update. Is trainer unique?";
        }

        return "success";
    }

    public String deleteStadium(String stadiumCity) {
        StadiumDTO stadiumDto = stadiumService.getStadiumByCity(stadiumCity);

        if (stadiumDto == null) {
            return "Stadium doesn't exist";
        }
        // we need to delete all associated badges
        List<BadgeDTO> stadBadges = badgeService.getAllBadges();
        List<Long> ids = new ArrayList<>();
        // fill list
        for (BadgeDTO badge : stadBadges) {
            if (badge.getStadium().equals(stadiumDto)) {
                ids.add(badge.getId());
            }
        }

        // could be more effective if we created method deleting all ids in one query
        for (Long delId : ids) {
            badgeService.delete(delId);
        }

        stadiumService.delete(stadiumDto.getId());
        return "success";
    }

    //POKEMON HANDLER AND METHODS
    @HandlesEvent("pokemons")
    public Resolution pokemons() {
        String method = getContext().getRequest().getMethod();
        String jo = "";

        if (!isAuthenticated()) {
            return new JSONResolution("Not authenticated.");
        }

        if (method.equals("GET")) {
            if (Long.parseLong(id) > 0) {          // id is in fact trainer id in this case
                jo = returnPokemonList(Long.parseLong(id));
            }
        }

        return new JSONResolution(jo);
    }

    public String returnPokemonList(Long trainerId) {
        JsonArrayBuilder ja = Json.createArrayBuilder();
        JsonObjectBuilder jb = Json.createObjectBuilder();

        TrainerDTO trainerDto = trainerService.getTrainerById(trainerId);
        if (trainerDto == null) {
            return "Trainer not found.";
        }

        List<PokemonDTO> pokemons = pokemonService.getPokemonsOfTrainer(trainerDto);
        if (pokemons.isEmpty()) {
            return "Trainer has no pokemons.";
        }

        for (PokemonDTO pokemonDto : pokemons) {
            ja.add(Json.createObjectBuilder()
                    .add("name", pokemonDto.getName())
                    .add("nickname", pokemonDto.getNickname())
                    .add("lvl", pokemonDto.getLvl())
                    .add("type", pokemonDto.getType().toString()));
        }
        jb.add("pokemons", ja.build());
        JsonObject jo = jb.build();

        return "success: " + jo.toString();
    }

    //BADGE HANDLER AND METHODS
    @HandlesEvent("badges")
    public Resolution badges() {
        String method = getContext().getRequest().getMethod();
        String jo = "";

        if (!isAuthenticated()) {
            return new JSONResolution("Not authenticated.");
        }

        if (method.equals("GET")) {
            if (Long.parseLong(id) > 0) {          // id is in fact trainer id in this case
                jo = returnBadgeList(Long.parseLong(id));
            }
        }

        return new JSONResolution(jo);
    }

    public String returnBadgeList(Long trainerId) {
        JsonArrayBuilder ja = Json.createArrayBuilder();
        JsonObjectBuilder jb = Json.createObjectBuilder();

        TrainerDTO trainerDto = trainerService.getTrainerById(trainerId);
        if (trainerDto == null) {
            return "Trainer not found.";
        }

        List<BadgeDTO> badges = badgeService.getAllTrainerBadges(trainerDto);
        if (badges.isEmpty()) {
            return "Trainer has no badges.";
        }

        for (BadgeDTO badgeDto : badges) {
            ja.add(Json.createObjectBuilder()
                    .add("stadium", badgeDto.getStadium().getCity())
                    .add("leader", badgeDto.getStadium().getLeader().getName()));
        }
        jb.add("badges", ja.build());
        JsonObject jo = jb.build();

        return "success: " + jo.toString();
    }

    public static Map<String, String> splitQuery(String query) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<>();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        //fill up the data for the table if validation errors occured
        trainers = trainerService.getAllTrainers();
        badges = badgeService.getAllBadges();
        pokemons = pokemonService.getAllPokemons();
        stadiums = stadiumService.getAllStadiums();
        //return null to let the event handling continue
        return null;
    }
}
