/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon.util;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sourceforge.stripes.action.Resolution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Jettro Coenradie (griddshore.nl, 2008)
 */
public class JSONResolution implements Resolution {
    private static final Logger logger = LoggerFactory.getLogger(JSONResolution.class);
    private static final String CONTENT_TYPE = "application/json";
    private String toPrint;
 
    public JSONResolution(String toPrint) {
        this.toPrint = toPrint;
    }
 
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType(CONTENT_TYPE);
        doExecute(request, response);
        response.getOutputStream().print(toPrint);
    }
 
    protected void doExecute(HttpServletRequest request, HttpServletResponse response) throws IOException {
    }
 
    protected String getToPrint() {
        return toPrint;
    }
 
    protected void setToPrint(String toPrint) {
        this.toPrint = toPrint;
    }
}
