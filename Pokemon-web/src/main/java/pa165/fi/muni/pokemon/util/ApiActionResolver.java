/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon.util;

import org.apache.commons.lang3.text.WordUtils; 
import net.sourceforge.stripes.action.ActionBean; 
import net.sourceforge.stripes.action.ActionBeanContext; 
import net.sourceforge.stripes.controller.NameBasedActionResolver; 

// jamesjory.com
public class ApiActionResolver extends NameBasedActionResolver { 
    @Override public String getEventName(Class<? extends ActionBean> bean, ActionBeanContext context) { 
        String method = context.getRequest().getMethod(); 
        return "rest" + WordUtils.capitalizeFully(method); 
    } 
}
