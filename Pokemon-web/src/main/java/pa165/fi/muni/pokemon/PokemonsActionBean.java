package pa165.fi.muni.pokemon;

import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import pa165.fi.muni.pokemon.dto.PokemonDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.service.PokemonService;
import pa165.fi.muni.pokemon.service.TrainerService;

/**
 *
 * @author Gabriel
 */
@UrlBinding("/pokemons/{$event}/{pokemon.id}")
public class PokemonsActionBean extends BaseActionBean implements ValidationErrorHandler {

    final static Logger log = LoggerFactory.getLogger(TrainersActionBean.class);

    @SpringBean //Spring can inject even to private and protected fields
    protected TrainerService trainerService;

    @SpringBean //Spring can inject even to private and protected fields
    protected PokemonService pokemonService;

    /* --- part for showing a list of badges ---- */
    private List<PokemonDTO> pokemons;
    private List<TrainerDTO> trainers;

    private TrainerDTO trainer;

    public List<PokemonDTO> getPokemons() {
        return pokemons;
    }

    public List<TrainerDTO> getTrainers() {
        return trainers;
    }

    public TrainerDTO getTrainer() {
        return trainer;
    }

    public void setTrainer(TrainerDTO trainer) {
        this.trainer = trainer;
    }

    @ValidateNestedProperties(value = {
        @Validate(on = {"add"}, field = "name", required = true, minlength = 2),
        @Validate(on = {"add"}, field = "nickname", required = true, minlength = 2),
        @Validate(on = {"add"}, field = "lvl", required = true, minvalue = 0, maxvalue = 100),})
    private PokemonDTO pokemon;

    public PokemonDTO getPokemon() {
        return pokemon;
    }

    public void setPokemon(PokemonDTO pokemon) {
        this.pokemon = pokemon;
    }

    @DefaultHandler
    public Resolution list() {
        log.debug("list()");
        trainers = trainerService.getAllTrainers();
        pokemons = pokemonService.getAllPokemons();
        return new ForwardResolution("/pokemon/list.jsp");
    }

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"pokemonsList"})
    public void loadTrainerFromDatabase() {
        String ids = getContext().getRequest().getParameter("pokemon.owner.id");
        if (ids == null) {
            return;
        }
        trainer = trainerService.getTrainerById(Long.parseLong(ids));
    }

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"add"})
    public void loadTrainerFromDatabaseAdd() {
        String name = getContext().getRequest().getParameter("pokemon.owner.name");
        if (name == null) {
            return;
        }
        trainer = trainerService.getTrainerByName(name);
    }

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"edit"})
    public void loadPokemonFromDatabase() {
        String ids = getContext().getRequest().getParameter("pokemon.id");
        if (ids == null) {
            return;
        }
        pokemon = pokemonService.getPokemonById(Long.parseLong(ids));
    }

    public Resolution add() {
        log.debug("add() pokemon={}", pokemon);
        pokemonService.create(trainer, pokemon.getName(), pokemon.getNickname(), pokemon.getType(), pokemon.getLvl());
        getContext().getMessages().add(new LocalizableMessage("pokemon.add.message", escapeHTML(pokemon.getNickname())));
        if (getContext().getRequest().getParameter("pokemon.owner.id") != null) {
            return new RedirectResolution(this.getClass(), "pokemonsList").addParameter("pokemon.owner.id", trainer.getId());
        } else {
            return new RedirectResolution(this.getClass(), "list");
        }
    }

    public Resolution pokemonsList() {
        log.debug("pokemonList()");
        pokemons = pokemonService.getPokemonsOfTrainer(trainer);
        return new ForwardResolution("/pokemon/list.jsp");
    }

    public Resolution edit() {
        log.debug("edit() pokemon={}", pokemon);
        trainers = trainerService.getAllTrainers();
        return new ForwardResolution("/pokemon/edit.jsp");
    }

    public Resolution save() {
        log.debug("save() pokemon={}", pokemon);

        String trainerName = pokemon.getOwner().getName();
        TrainerDTO owner = trainerService.getTrainerByName(trainerName);
        pokemon.setOwner(owner);

        pokemonService.update(pokemon);
        return new RedirectResolution(this.getClass(), "list");
    }

    public Resolution delete() {
        pokemon = pokemonService.getPokemonById(pokemon.getId());

        log.debug("delete({})", pokemon.getId());
        pokemonService.delete(pokemon.getId());
        getContext().getMessages().add(new LocalizableMessage("pokemon.delete.message", escapeHTML(pokemon.getName())));
        return new RedirectResolution(this.getClass(), "list");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        /* fill up the data for the table if validation errors occured */
        if (trainer != null && getContext().getRequest().getParameter("pokemon.owner.id") != null) {
            pokemons = pokemonService.getPokemonsOfTrainer(trainer);

        } else {
            pokemons = pokemonService.getAllPokemons();
        }

        /* return null to let the event handling continue */
        return null;
    }
}
