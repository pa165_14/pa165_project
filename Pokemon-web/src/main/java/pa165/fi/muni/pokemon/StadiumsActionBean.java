package pa165.fi.muni.pokemon;

import java.util.ArrayList;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import pa165.fi.muni.pokemon.dto.BadgeDTO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.service.BadgeService;
import pa165.fi.muni.pokemon.service.StadiumService;
import pa165.fi.muni.pokemon.service.TrainerService;

/**
 *
 * @author Kuba
 */
@UrlBinding("/stadiums/{$event}/{stadium.id}")
public class StadiumsActionBean extends BaseActionBean implements ValidationErrorHandler {

    final static Logger log = LoggerFactory.getLogger(StadiumsActionBean.class);

    @SpringBean //Spring can inject even to private and protected fields
    protected StadiumService stadiumService;

    @SpringBean //Spring can inject even to private and protected fields
    protected TrainerService trainerService;

    @SpringBean //Spring can inject even to private and protected fields
    protected BadgeService badgeService;

    /* part for showing a list of stadiums */
    private List<StadiumDTO> stadiums;
    private List<TrainerDTO> trainers;

    private String id;

    /*
     * User cannot see stadiums
     */
    public Boolean checkAccess() {
        if (getRoleName().equals("user")) {
            return false;
        }

        return true;
    }

    @DefaultHandler
    public Resolution list() {
        log.debug("list()");
        if (!checkAccess()) {
            return new RedirectResolution("/index.jsp");
        }

        stadiums = stadiumService.getAllStadiums();
        trainers = trainerService.getAllTrainers();
        return new ForwardResolution("/stadium/list.jsp");
    }

    public List<StadiumDTO> getStadiums() {
        return stadiums;
    }

    public List<TrainerDTO> getTrainers() {
        return trainers;
    }

    /* add stadium */
    @ValidateNestedProperties(value = {
        @Validate(on = {"add", "save"}, field = "city", required = true, minlength = 1),
        @Validate(on = {"add", "save"}, field = "leader.id", required = true, minvalue = 1),})
    private StadiumDTO stadium;

    public Resolution add() {
        log.debug("add() stadium={}", stadium);
        log.debug("hello");
        if (!checkAccess()) {
            return new RedirectResolution("/index.jsp");
        }

        if (stadiumService.getStadiumByCity(stadium.getCity()) != null) {
            getContext().getMessages().add(new LocalizableMessage("stadium.notunique.message", escapeHTML(stadium.getCity())));
            return new RedirectResolution(this.getClass(), "list");
        }

        TrainerDTO trainer = trainerService.getTrainerById(stadium.getLeader().getId());
        stadiumService.create(trainer, stadium.getCity(), stadium.getType());
        getContext().getMessages().add(new LocalizableMessage("stadium.add.message", escapeHTML(stadium.getCity())));
        return new RedirectResolution(this.getClass(), "list");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        /* fill up the data for the table if validation errors occured */
        stadiums = stadiumService.getAllStadiums();

        /* return null to let the event handling continue */
        return null;
    }

    public StadiumDTO getStadium() {
        return stadium;
    }

    public void setStadium(StadiumDTO stadium) {
        this.stadium = stadium;
    }

    /* delete stadium */
    public Resolution delete() {
        log.debug("delete({})", stadium.getId());
        if (!checkAccess()) {
            return new RedirectResolution("/index.jsp");
        }

        //only id is filled by the form
        stadium = stadiumService.getStadiumByCity(stadium.getCity());

        // we need to delete all associated badges
        List<BadgeDTO> badges = badgeService.getAllBadges();
        List<Long> ids = new ArrayList<>();
        // fill list
        for (BadgeDTO badge : badges) {
            if (badge.getStadium().equals(stadium)) {
                ids.add(badge.getId());
            }
        }

        // could be more effective if we created method deleting all ids in one query
        for (Long delId : ids) {
            badgeService.delete(delId);
        }

        stadiumService.delete(stadium.getId());
        getContext().getMessages().add(new LocalizableMessage("stadium.delete.message", escapeHTML(stadium.getCity())));
        return new RedirectResolution(this.getClass(), "list");
    }

    /* edit stadium */
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"edit", "save"})
    public void loadStadiumFromDatabase() {
        String city = getContext().getRequest().getParameter("stadium.city");
        if (city == null) {
            return;
        }
        stadium = stadiumService.getStadiumByCity(city);
    }

    public Resolution edit() {
        log.debug("edit() stadium={}", stadium);
        if (!checkAccess()) {
            return new RedirectResolution("/index.jsp");
        }

        trainers = trainerService.getAllTrainers();
        return new ForwardResolution("/stadium/edit.jsp");
    }

    public Resolution save() {
        log.debug("save() stadium={}", stadium);
        if (!checkAccess()) {
            return new RedirectResolution("/index.jsp");
        }

        stadiumService.update(stadium);
        return new RedirectResolution(this.getClass(), "list");
    }
}
