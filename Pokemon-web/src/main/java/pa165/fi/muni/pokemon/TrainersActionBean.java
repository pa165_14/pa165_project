package pa165.fi.muni.pokemon;

import java.util.ArrayList;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Date;
import java.util.List;

import net.sourceforge.stripes.validation.DateTypeConverter;
import pa165.fi.muni.pokemon.dto.BadgeDTO;
import pa165.fi.muni.pokemon.dto.PokemonDTO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.service.BadgeService;
import pa165.fi.muni.pokemon.service.PokemonService;
import pa165.fi.muni.pokemon.service.StadiumService;
import pa165.fi.muni.pokemon.service.TrainerService;

/**
 *
 * @author Kuba
 */
@UrlBinding("/trainers/{$event}/{trainer.id}")
public class TrainersActionBean extends BaseActionBean implements ValidationErrorHandler {

    final static Logger log = LoggerFactory.getLogger(TrainersActionBean.class);

    @SpringBean //Spring can inject even to private and protected fields
    protected TrainerService trainerService;
    
    @SpringBean //Spring can inject even to private and protected fields
    protected StadiumService stadiumService;

    @SpringBean //Spring can inject even to private and protected fields
    protected BadgeService badgeService;
    
    @SpringBean //Spring can inject even to private and protected fields
    protected PokemonService pokemonService;

    /* --- part for showing a list of trainers ---- */
    private List<TrainerDTO> trainers;
    private List<BadgeDTO> badges;
    private List<PokemonDTO> pokemons;
    private List<StadiumDTO> stadiums;

    public List<TrainerDTO> getTrainers() {
        return trainers;
    }
    public List<BadgeDTO> getBadges() {
        return badges;
    }
    public List<PokemonDTO> getPokemons() {
        return pokemons;
    }
    public List<StadiumDTO> getStadiums() {
        return stadiums;
    }
    
    /* --- part for adding a trainer ---- */

    @ValidateNestedProperties(value = {
            @Validate(on = {"add", "save"}, field = "name", required = true),
    })
    private TrainerDTO trainer;
    
    public TrainerDTO getTrainer() {
        return trainer;
    }

    public void setTrainer(TrainerDTO trainer) {
        this.trainer = trainer;
    }
    
    @DefaultHandler
    public Resolution list() {
        log.debug("list()");
        trainers = trainerService.getAllTrainers();
        badges = badgeService.getAllBadges();
        pokemons = pokemonService.getAllPokemons();
        stadiums = stadiumService.getAllStadiums();
        return new ForwardResolution("/trainer/list.jsp");
    }

    public Resolution add() {
        log.debug("add() trainer={}", trainer);

        // validate on uniqueness
        TrainerDTO tr = trainerService.getTrainerByName(trainer.getName());
        if (tr != null) {
            getContext().getMessages().add(new LocalizableMessage("trainer.list.name_already_exists"));
            return new RedirectResolution(this.getClass(), "list");
        }
        
        trainerService.create(trainer.getName(), trainer.getBirth());
        getContext().getMessages().add(new LocalizableMessage("trainer.add.message",escapeHTML(trainer.getName())));
        return new RedirectResolution(this.getClass(), "list");
    }

    //--- part for deleting a trainer ----
    public Resolution delete() {
        //ValidationErrors errors = new ValidationErrors();
        log.debug("delete({})", trainer.getId());

        //only id is filled by the form
        trainer = trainerService.getTrainerById(trainer.getId());
        
        if (trainer.getStadium() != null) {
            getContext().getMessages().add(new LocalizableMessage("trainer.list.trainer_leads_stadium", escapeHTML(trainer.getStadium().getCity())));
            return new RedirectResolution(this.getClass(), "list");
        }

        // we need to delete all associated badges
        List<BadgeDTO> badges = badgeService.getAllBadges();
        List<Long> ids = new ArrayList<Long>();
        // fill list
        for (BadgeDTO badge : badges) {
            if (badge.getHolder().equals(trainer)) {
                ids.add(badge.getId());
            }
        }
        // could be more effective if we created method deleting all ids in one query
        for (Long delId : ids) {
            badgeService.delete(delId);
        }
        
        ids.clear();
        List<PokemonDTO> pokemons = pokemonService.getPokemonsOfTrainer(trainer);
        for (PokemonDTO pokemon : pokemons) {
            ids.add(pokemon.getId());
        }
        for (Long delId : ids) {
            pokemonService.delete(delId);
        }
        
        trainerService.delete(trainer.getId());
        getContext().getMessages().add(new LocalizableMessage("trainer.delete.message", escapeHTML(trainer.getName())));
        return new RedirectResolution(this.getClass(), "list");
    }
    
    public Resolution enterToLeague() {
        Boolean result = trainerService.enterToLeague(trainer);
        if (result) {
            getContext().getMessages().add(new LocalizableMessage("trainer.list.you_are_prepared", escapeHTML(trainer.getName())));
        } else {
            getContext().getMessages().add(new LocalizableMessage("trainer.list.you_are_not_prepared", escapeHTML(trainer.getName())));
        }
        return new RedirectResolution(this.getClass(), "list");
    }

    //--- part for editing a trainer ----
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"edit", "save"})
    public void loadTrainerFromDatabase() {
        String ids = getContext().getRequest().getParameter("trainer.id");
        if (ids == null) return;
        trainer = trainerService.getTrainerById(Long.parseLong(ids));
    }

    public Resolution edit() {
        log.debug("edit() trainer={}", trainer);
        return new ForwardResolution("/trainer/edit.jsp");
    }

    public Resolution save() {
        log.debug("save() trainer={}", trainer);
        trainerService.update(trainer);
        return new RedirectResolution(this.getClass(), "list");
    }
    
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"defeat"})
    public void loadTrainerFromDatabaseDef() {
        String name = getContext().getRequest().getParameter("trainer.name");
        String city = getContext().getRequest().getParameter("stadium.city");
        if (name == null || city == null) return;
        trainer = trainerService.getTrainerByName(name);
    }
    
    public Resolution defeat() {
        log.debug("defeat() trainer={}", trainer);
        boolean defeat = trainerService.defeat(trainer, stadiumService.getStadiumByCity(getContext().getRequest().getParameter("stadium.city")));
        if (defeat == true) {
            getContext().getMessages().add(new LocalizableMessage("trainer.defeat.true.message"));
            return new RedirectResolution(BadgesActionBean.class, "trainersList").addParameter("badge.holder.id", trainer.getId());
        } else {
            getContext().getMessages().add(new LocalizableMessage("trainer.defeat.false.message"));
            return new RedirectResolution(this.getClass(), "list");
        }
    } 
    
    private Date birth;
    @Validate(converter=DateTypeConverter.class)
    public void setBirth(Date birth) {
      this.birth = birth;
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        /* fill up the data for the table if validation errors occured */
        trainers = trainerService.getAllTrainers();
        badges = badgeService.getAllBadges();
        pokemons = pokemonService.getAllPokemons();
        stadiums = stadiumService.getAllStadiums();

        /* return null to let the event handling continue */
        return null;
    }
}
