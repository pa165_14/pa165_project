package pa165.fi.muni.pokemon;

import java.util.HashMap;
import java.util.Map;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.LocalizableMessage;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import org.apache.taglibs.standard.functions.Functions;

/**
 * Base actionBean implementing the required methods for setting and getting
 * context.
 *
 * @author Petr Bartusek
 */
public class BaseActionBean implements ActionBean {

    private ActionBeanContext context;
    
    private final Map<Integer, String> roles = new HashMap<Integer, String>() {
        {
            put(0, "User");
            put(2, "Leader");
            put(5, "Admin");
        }
    };    
    
    public Map<Integer, String> getRoles() {
        return roles;
    }
    
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }
    
    @Override
    public ActionBeanContext getContext() {
        return context;
    }
    
    public Resolution roleName() {
        String roleName = getContext().getRequest().getParameter("roleName");
        getContext().getRequest().getSession().setAttribute("roleName", roleName);
        return new RedirectResolution("/index.jsp");
    }
    
    /*
     * returns role name in session or first role ("user") if undefined
     */
    public String getRoleName() {
        return getContext().getRequest().getSession().getAttribute("roleName") != null
                ? (String) getContext().getRequest().getSession().getAttribute("roleName") : roles.get(0).toLowerCase();
    }
    
    public static String escapeHTML(String s) {
        return Functions.escapeXml(s);
    }
}
