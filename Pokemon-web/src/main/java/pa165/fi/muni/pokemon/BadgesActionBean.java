package pa165.fi.muni.pokemon;

import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.LocalizableMessage;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pa165.fi.muni.pokemon.dto.BadgeDTO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.exception.EntityAlreadyExists;
import pa165.fi.muni.pokemon.service.BadgeService;
import pa165.fi.muni.pokemon.service.StadiumService;
import pa165.fi.muni.pokemon.service.TrainerService;

/**
 *
 * @author Petr
 */
@UrlBinding("/badges/{$event}/{badge.holder.id}")
public class BadgesActionBean extends BaseActionBean implements ValidationErrorHandler {

    final static Logger log = LoggerFactory.getLogger(TrainersActionBean.class);

    @SpringBean //Spring can inject even to private and protected fields
    protected StadiumService stadiumService;

    @SpringBean //Spring can inject even to private and protected fields
    protected TrainerService trainerService;

    @SpringBean //Spring can inject even to private and protected fields
    protected BadgeService badgeService;

    /* part for showing a list of badges */
    private List<BadgeDTO> badges;
    private List<StadiumDTO> stadiums;
    private List<TrainerDTO> trainers;

    private TrainerDTO trainer;

    public List<BadgeDTO> getBadges() {
        return badges;
    }

    public List<StadiumDTO> getStadiums() {
        return stadiums;
    }

    public List<TrainerDTO> getTrainers() {
        return trainers;
    }

    public TrainerDTO getTrainer() {
        return trainer;
    }

    public void setTrainer(TrainerDTO trainer) {
        this.trainer = trainer;
    }

    @ValidateNestedProperties(value = {
        @Validate(on = {"add"}, field = "holder.name", required = true, minlength = 2),})
    private BadgeDTO badge;

    public BadgeDTO getBadge() {
        return badge;
    }

    public void setBadge(BadgeDTO badge) {
        this.badge = badge;
    }

    @DefaultHandler
    public Resolution list() {
        log.debug("list()");
        trainers = trainerService.getAllTrainers();
        badges = badgeService.getAllBadges();
        stadiums = stadiumService.getAllStadiums();
        return new ForwardResolution("/badge/list.jsp");
    }

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"trainersList"})
    public void loadTrainerFromDatabase() {
        String ids = getContext().getRequest().getParameter("badge.holder.id");
        if (ids == null) {
            return;
        }
        trainer = trainerService.getTrainerById(Long.parseLong(ids));
    }

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"add"})
    public void loadTrainerFromDatabaseAdd() {
        String ids = getContext().getRequest().getParameter("badge.holder.id");
        if (ids == null) {
            String name = getContext().getRequest().getParameter("badge.holder.name");
            if (name == null) {
                return;
            }
            trainer = trainerService.getTrainerByName(name);
        } else {
            trainer = trainerService.getTrainerById(Long.parseLong(ids));
        }
    }

    public Resolution trainersList() {
        log.debug("trainersList()");
        badges = badgeService.getAllTrainerBadges(trainer);
        stadiums = stadiumService.getAllStadiums();
        return new ForwardResolution("/badge/list.jsp");
    }

    public Resolution add() {
        log.debug("add() badge={}", badge);
        StadiumDTO stadium = stadiumService.getStadiumByCity(badge.getStadium().getCity());
        try {
            badgeService.create(trainer, stadium);

            getContext().getMessages().add(new LocalizableMessage("badge.add.message"));
            if (getContext().getRequest().getParameter("badge.holder.id") != null || getContext().getRequest().getParameter("badge.holder.name") != null) {
                return new RedirectResolution(this.getClass(), "trainersList").addParameter("badge.holder.id", trainer.getId());
            } else {
                return new RedirectResolution(this.getClass(), "list");
            }
        } catch (EntityAlreadyExists ex) {
            getContext().getMessages().add(new LocalizableMessage("badge.exists.message"));
            return new RedirectResolution(this.getClass(), "list");
        }
    }

    public Resolution delete() {
        log.debug("delete({})", badge.getId());
        badgeService.delete(badge.getId());
        getContext().getMessages().add(new LocalizableMessage("badge.delete.message"));
        return new RedirectResolution(this.getClass(), "list");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        /* fill up the data for the table if validation errors occured */
        if (trainer != null && getContext().getRequest().getParameter("badge.holder.ex") != null) {
            badges = badgeService.getAllTrainerBadges(trainer);
        } else {
            badges = badgeService.getAllBadges();
        }

        /* return null to let the event handling continue */
        return null;
    }
}
