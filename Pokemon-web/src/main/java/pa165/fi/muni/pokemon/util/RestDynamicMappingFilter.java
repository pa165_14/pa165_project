/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon.util;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sourceforge.stripes.controller.DynamicMappingFilter;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author jamesjory.com
 */
public class RestDynamicMappingFilter extends DynamicMappingFilter { 
    @Override public void doFilter(ServletRequest request, ServletResponse response, final FilterChain chain) throws IOException, ServletException { 
        super.doFilter(request, response, new FilterChain() { 
            @Override public void doFilter(ServletRequest request, ServletResponse response) throws IOException, ServletException { 
                String method = StringUtils.upperCase(((HttpServletRequest)request).getMethod()); 
                if ("DELETE".equals(method) || "PUT".equals(method)) 
                    ((HttpServletResponse)response).sendError(HttpServletResponse.SC_NOT_FOUND); 
                else chain.doFilter(request, response); 
            } 
        }); 
    } 
}