<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<s:layout-definition>
    <!DOCTYPE html>
    <html lang="${pageContext.request.locale}">
        <head>
            <!-- Basic Page Needs
                –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <meta charset="utf-8">
            <title><f:message key="${titlekey}"/></title>
            <meta name="description" content="Pokemon League Manager">
            <meta name="author" content="Petr Bartusek, Gabriel Tóth, Jakub Vonšovký">
            <!-- Mobile Specific Metas
            –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- FONT
            –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <link href='//fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'>

            <!-- CSS
            –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <!--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/skeleton/normalize.css" />
            <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/skeleton/skeleton.css">-->
            <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/custom.css" />
            <c:choose>
                <c:when test="${roleName == 'admin'}">
                    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style2.css" />
                </c:when>
                <c:when test="${roleName == 'loggeduser'}">
                    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style2.css" />
                </c:when>
                <c:otherwise>
                    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
                </c:otherwise>
            </c:choose>
            <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery-ui.min.css">

            <!-- SCRIPT
            –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <script src="${pageContext.request.contextPath}/js/jquery.js"></script>
            <script src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>

            <!-- Favicon
             –––––––––––––––––––––––––––––––––––––––––––––––––– -->
            <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/images/favicon.png" />

            <s:layout-component name="header"/>
        </head>
        <body>
            <h1 id="title"><f:message key="${titlekey}"/></h1>
            <div id="navigation">
                <s:form beanclass="pa165.fi.muni.pokemon.BaseActionBean">                   
                    <select name="roleName" style="float: right; margin-right: 10px;" onchange="this.form.submit();">
                        <c:choose>
                            <c:when test="${roleName == 'admin'}">
                                <option value="user">User</option>
                                <option value="loggeduser">Logged User</option>
                                <option value="admin" selected="selected">Admin</option>
                            </c:when>
                            <c:when test="${roleName == 'loggeduser'}">
                                <option value="user">User</option>
                                <option value="loggeduser" selected="selected">Logged User</option>
                                <option value="admin">Admin</option>
                            </c:when>
                            <c:otherwise>
                                <option value="user" selected="selected">User</option>
                                <option value="loggeduser">Logged User</option>
                                <option value="admin">Admin</option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </s:form>

                <ul>
                    <c:choose>       
                        <c:when test="${roleName == 'admin'}">
                            <li><s:link href="/index.jsp"><f:message key="navigation.index"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.TrainersActionBean" event="list"><f:message key="navigation.trainers"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.StadiumsActionBean" event="list"><f:message key="navigation.stadiums"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.BadgesActionBean" event="list"><f:message key="navigation.badges"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.PokemonsActionBean" event="list"><f:message key="navigation.pokemons"/></s:link></li>
                        </c:when>
                        <c:when test="${roleName == 'loggeduser'}">
                            <li><s:link href="/index.jsp"><f:message key="navigation.index"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.TrainersActionBean" event="list"><f:message key="navigation.trainers"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.StadiumsActionBean" event="list"><f:message key="navigation.stadiums"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.BadgesActionBean" event="list"><f:message key="navigation.badges"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.PokemonsActionBean" event="list"><f:message key="navigation.pokemons"/></s:link></li>
                        </c:when>
                        <c:otherwise>
                            <li><s:link href="/index.jsp"><f:message key="navigation.index"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.TrainersActionBean" event="list"><f:message key="navigation.trainers"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.BadgesActionBean" event="list"><f:message key="navigation.badges"/></s:link></li>
                            <li><s:link beanclass="pa165.fi.muni.pokemon.PokemonsActionBean" event="list"><f:message key="navigation.pokemons"/></s:link></li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>  
                <div id="content">
                    <div id="insidecontent">
                    <s:messages/>
                    <s:layout-component name="body"/>
                </div>
            </div>
        </body>
    </html>
</s:layout-definition>