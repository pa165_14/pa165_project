<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="badge.list.title">
    <s:layout-component name="body">
        <s:useActionBean beanclass="pa165.fi.muni.pokemon.BadgesActionBean" var="actionBean"/>

        <p id="texts">
            <f:message key="badge.list.all"/>
        </p>

        <c:set var="deleteconfirm" scope="page">
            <f:message key="badge.list.deleteconfirm" />
        </c:set>

            <table class="basic">
                <thead>
                    <tr>
                        <th><f:message key="badge.holder.id"/></th>
                    <th><f:message key="badge.holder.name"/></th>
                    <th><f:message key="badge.stadium.city"/></th>
                    <c:if test="${actionBean.roleName == 'admin'}">
                        <th></th>
                    </c:if>
                    
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${actionBean.badges}" var="badge">
                    <tr>
                        <td>${badge.id}</td>
                        <td>${badge.holder.name}</td>
                        <td>${badge.stadium.city}</td>
                        <c:if test="${actionBean.roleName == 'admin'}">
                            <td>
                                <s:form beanclass="pa165.fi.muni.pokemon.BadgesActionBean">
                                    <s:hidden name="badge.id" value="${badge.id}"/>
                                    <s:submit name="delete" onclick="return confirm('${pageScope.deleteconfirm}');">
                                        <f:message key="badge.list.delete"/>
                                    </s:submit>
                                </s:form>
                            </td>
                        </c:if>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <c:if test="${actionBean.roleName == 'admin'}">
        <div id="mydiv">
            <s:form beanclass="pa165.fi.muni.pokemon.BadgesActionBean">
                <fieldset id="createfield">
                    <legend id="texts">
                        <f:message key="badge.list.newBadge"/>
                    </legend>
                    <%@include file="form.jsp"%>
                    <s:submit class="myButton2" name="add">
                        <f:message key="badge.list.createBadge"/>
                    </s:submit>
                </fieldset>
            </s:form>
        </div>
        </c:if>
    </s:layout-component>
</s:layout-render>