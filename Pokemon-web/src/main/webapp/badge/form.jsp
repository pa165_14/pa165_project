<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>



<table id="t2">        
    <c:choose>
        <c:when test="${actionBean.trainer.id == null}">
            <tr>
                <th><s:label for="bOwner" name="badge.holder.name"/></th>
                <td><s:select id="bOwner" name="badge.holder.name"><s:options-collection collection="${actionBean.trainers}" label="name" value="name" /></s:select></td>
            </tr>
        </c:when>
        <c:otherwise>
            <s:hidden name="badge.holder.name" value="${actionBean.trainer.name}"/>
            <s:hidden name="badge.holder.ex" value="ex"/>
        </c:otherwise>
    </c:choose>  
    <tr>
        <th><s:label for="bStadium" name="badge.stadium.city"/></th>
        <td><s:select id="bStadium" name="badge.stadium.city"><s:options-collection collection="${actionBean.stadiums}" label="city" value="city" /></s:select></td>
    </tr>
</table>