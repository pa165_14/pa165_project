<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>

<table id="t2">
    <c:choose>
        <c:when test="${actionBean.trainer.id == null}">
            <tr>
                <th><s:label for="bOwner" name="pokemon.owner.name"/></th>
                <td><s:select id="bOwner" name="pokemon.owner.name"><s:options-collection collection="${actionBean.trainers}" label="name" value="name" /></s:select></td>
            </tr>
        </c:when>
        <c:otherwise>
            <s:hidden name="pokemon.owner.id" value="${actionBean.trainer.id}" />
            <s:hidden name="pokemon.owner.name" value="${actionBean.trainer.name}" />
        </c:otherwise>
    </c:choose> 

    <tr>
        <th><s:label for="b1" name="pokemon.name" /></th>
        <td><s:text id="b1" name="pokemon.name" /></td>
    </tr>
    <tr>
        <th><s:label for="b2" name="pokemon.nickname" /></th>
        <td><s:text id="b2" name="pokemon.nickname" /></td>
    </tr>
        <tr>
        <th><s:label for="b3" name="pokemon.lvl" /></th>
        <td><s:text id="b3" name="pokemon.lvl" /></td>
    </tr>
    <tr>
        <th><s:label for="b4" name="pokemon.type"/></th>
        <td><s:select id="b4" name="pokemon.type"><s:options-enumeration enum="pa165.fi.muni.pokemon.entity.Type" /></s:select></td>
    </tr>
</table>