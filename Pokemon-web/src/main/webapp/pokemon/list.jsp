<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="pokemon.list.title">
    <s:layout-component name="body">
        <s:useActionBean beanclass="pa165.fi.muni.pokemon.PokemonsActionBean" var="actionBean"/>

        <p id="texts"><f:message key="pokemon.list.all"/></p>

        <c:set var="deleteconfirm" scope="page">
            <f:message key="pokemon.list.deleteconfirm" />
        </c:set>
        <table class="basic">
            <thead>
                <tr>
                    <th><f:message key="pokemon.id"/></th>
                    <th><f:message key="pokemon.name"/></th>
                    <th><f:message key="pokemon.owner.name"/></th>
                    <th><f:message key="pokemon.nickname"/></th>
                    <th><f:message key="pokemon.lvl"/></th>
                    <th><f:message key="pokemon.type"/></th>
                    <c:if test="${actionBean.roleName == 'admin'}">
                        <th></th>
                        <th></th>
                    </c:if>
                    <c:if test="${actionBean.roleName == 'loggeduser'}">
                        <th></th>
                    </c:if>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${actionBean.pokemons}" var="pokemon">
                    <tr>
                        <td><c:out value="${pokemon.id}"/></td>
                        <td><c:out value="${pokemon.name}"/></td>
                        <td><c:out value="${pokemon.owner.name}"/></td>
                        <td><c:out value="${pokemon.nickname}"/></td>
                        <td><c:out value="${pokemon.lvl}"/></td>
                        <td><c:out value="${pokemon.type}"/></td>
                        <c:if test="${actionBean.roleName == 'loggeduser' || actionBean.roleName == 'admin'}">
                        <td>
                            <s:link beanclass="pa165.fi.muni.pokemon.PokemonsActionBean" event="edit">
                                <s:param name="pokemon.id" value="${pokemon.id}"/>
                                <f:message key="pokemon.edit.edit"/>
                            </s:link>
                        </td>
                        </c:if>
                        <c:if test="${actionBean.roleName == 'admin'}">
                            <td>
                                <s:form beanclass="pa165.fi.muni.pokemon.PokemonsActionBean">
                                    <s:hidden name="pokemon.id" value="${pokemon.id}"/>
                                    <s:submit name="delete" onclick="return confirm('${pageScope.deleteconfirm}');">
                                        <f:message key="pokemon.list.delete"/>
                                    </s:submit>
                                </s:form>
                            </td>
                        </c:if>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <c:if test="${actionBean.roleName == 'admin' || actionBean.roleName == 'loggeduser'}">
            <div id="mydiv">
                <s:form beanclass="pa165.fi.muni.pokemon.PokemonsActionBean">
                    <fieldset id="createfield">
                        <legend id="texts">
                            <f:message key="pokemon.list.new"/>
                        </legend>
                        <%@include file="form.jsp"%>
                        <s:submit class="myButton2" name="add">
                            <f:message key="pokemon.list.create"/>
                        </s:submit>
                    </fieldset>
                </s:form>
            </div>
        </c:if>
    </s:layout-component>
</s:layout-render>