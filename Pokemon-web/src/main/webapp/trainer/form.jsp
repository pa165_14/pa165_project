<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>

<script>
    $(function () {
        $("#b2").datepicker();
        $("#b2").datepicker("option", "dateFormat", "dd.mm.yy");

        var b2val = $("#b2").attr("value");
        $("#b2").val(b2val);
    });
</script>

<table id="t2">
    <tr>
        <th>
            <s:label for="b1" name="trainer.name"/>
        </th>
        <td>
            <s:text id="b1" name="trainer.name"/>
        </td>
    </tr>
    <tr>
        <th>
            <s:label for="b2" name="trainer.birth" />
        </th>
        <td>
            <s:text id="b2" formatPattern="dd.MM.yyyy" name="trainer.birth" />
        </td>
    </tr>
</table>