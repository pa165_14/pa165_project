<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="trainer.list.title">
    <s:layout-component name="body">
        <s:useActionBean beanclass="pa165.fi.muni.pokemon.TrainersActionBean" var="actionBean"/>

        <p id="texts"><f:message key="trainer.list.alltrainers"/></p>

        <c:set var="deleteconfirm" scope="page">
            <f:message key="trainer.list.deleteconfirm" />
        </c:set>
        <c:set var="are_you_ready_for_league" scope="page">
            <f:message key="trainer.list.are_you_ready_for_league" />
        </c:set>

        <table class="basic">
            <thead>
                <tr>
                    <th>id</th>
                    <th><f:message key="trainer.name"/></th>
                    <th><f:message key="trainer.birth"/></th>
                    <th><f:message key="trainer.stadium"/></th>
                    <th><f:message key="trainer.badges"/></th>
                    <th><f:message key="trainer.pokemons"/></th>
                    <c:if test="${actionBean.roleName == 'admin'}">
                        <th></th>
                        <th></th>
                        <th></th>
                    </c:if>
                    <c:if test="${actionBean.roleName == 'loggeduser'}">
                        <th></th>
                    </c:if>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${actionBean.trainers}" var="trainer">
                    <tr>
                        <td>${trainer.id}</td>
                        <td><c:out value="${trainer.name}"/></td>
                        <td><f:formatDate value="${trainer.birth}" pattern="dd.MM.yyyy"/></td>
                        <td><c:out value="${trainer.stadium.city}"/></td>
                        <td>
                            <c:set var="count" value="0" scope="page" />
                            <c:forEach items="${actionBean.badges}" var="badge">
                                <c:if test="${badge.holder.name == trainer.name}">
                                    <c:set var="count" value="${count + 1}" scope="page"/>
                                </c:if>
                            </c:forEach>
                            <s:link beanclass="pa165.fi.muni.pokemon.BadgesActionBean" event="trainersList"><s:param name="badge.holder.id" value="${trainer.id}"/>
                                <c:out value="${count}" />   
                            </s:link>     
                        </td>
                        <td>
                            <c:set var="count" value="0" scope="page" />
                            <c:forEach items="${actionBean.pokemons}" var="pokemon">
                                <c:if test="${pokemon.owner.name == trainer.name}">
                                    <c:set var="count" value="${count + 1}" scope="page"/>
                                </c:if>
                            </c:forEach>
                            <s:link beanclass="pa165.fi.muni.pokemon.PokemonsActionBean" event="pokemonsList">
                                <s:param name="pokemon.owner.id" value="${trainer.id}"/>
                                <c:out value="${count}" />   
                            </s:link>     
                        </td>
                        <c:if test="${actionBean.roleName == 'loggeduser' || actionBean.roleName == 'admin'}">
                        <td>
                            <s:form beanclass="pa165.fi.muni.pokemon.TrainersActionBean">
                                <s:hidden name="trainer.id" value="${trainer.id}"/>
                                <s:submit class="mybutton2" name="enterToLeague" onclick="return confirm('${pageScope.are_you_ready_for_league}');">
                                    <f:message key="trainer.list.to_league"/>
                                </s:submit>
                            </s:form>
                        </td>
                        </c:if>
                        <c:if test="${actionBean.roleName == 'admin'}">
                        <td>
                            <s:link beanclass="pa165.fi.muni.pokemon.TrainersActionBean" event="edit"><s:param name="trainer.id" value="${trainer.id}"/>
                                <f:message key="trainer.edit.edit"/>
                            </s:link>
                            </td>
                            <td>
                            <s:form beanclass="pa165.fi.muni.pokemon.TrainersActionBean">
                                <s:hidden name="trainer.id" value="${trainer.id}"/>
                                <s:submit class="mybutton2" name="delete" onclick="return confirm('${pageScope.deleteconfirm}');"><f:message key="trainer.list.delete"/></s:submit>
                            </s:form>
                        </td>
                        </c:if>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div id="mydiv">
            <c:if test="${actionBean.roleName == 'admin'}">
            <s:form beanclass="pa165.fi.muni.pokemon.TrainersActionBean">
                <fieldset id="createfield">
                    <legend id="texts">
                        <f:message key="trainer.list.newtrainer"/>
                    </legend>
                    <%@include file="form.jsp"%>
                    <s:submit class="myButton2" name="add"><f:message key="trainer.list.createnew"/></s:submit>
                </fieldset>
            </s:form>
            </c:if>
            <c:if test="${actionBean.roleName == 'admin'}">
            <s:form beanclass="pa165.fi.muni.pokemon.TrainersActionBean">
                <fieldset style="margin-left:50px;" id="createfield">
                    <legend id="texts">
                        <f:message key="trainer.list.defeat"/>
                    </legend>
                    <%@include file="defForm.jsp"%>
                    <s:submit class="myButton2" name="defeat">
                        <f:message key="trainer.list.defeat"/>
                    </s:submit>
                </fieldset>
            </s:form>
            </c:if>
            <c:if test="${actionBean.roleName == 'loggeduser'}">
            <s:form beanclass="pa165.fi.muni.pokemon.TrainersActionBean">
                <fieldset id="createfield">
                    <legend id="texts">
                        <f:message key="trainer.list.defeat"/>
                    </legend>
                    <%@include file="defForm.jsp"%>
                    <s:submit class="myButton2" name="defeat">
                        <f:message key="trainer.list.defeat"/>
                    </s:submit>
                </fieldset>
            </s:form>
            </c:if>
        </div>
    </s:layout-component>
</s:layout-render>
