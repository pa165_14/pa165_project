<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<table id="t2">
    <tr>
        <th>
            <s:label for="bOwner" name="trainer.name"/>
        </th>
        <td>
            <s:select id="bOwner" name="trainer.name">
                <s:options-collection collection="${actionBean.trainers}" label="name" value="name" />
            </s:select>
        </td>
    </tr>
    <tr>
        <th>
            <s:label for="bStadium" name="stadium.city"/>
        </th>
        <td>
            <s:select id="bStadium" name="stadium.city">
                <s:options-collection collection="${actionBean.stadiums}" label="city" value="city" />
            </s:select>
        </td>
    </tr>
</table>