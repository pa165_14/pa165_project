<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="stadium.list.title">
    <s:layout-component name="body">
        <s:useActionBean beanclass="pa165.fi.muni.pokemon.StadiumsActionBean" var="actionBean"/>

        <p id="texts">
            <f:message key="stadium.list.allstadiums"/>
        </p>

        <c:set var="deleteconfirm" scope="page">
            <f:message key="stadium.list.deleteconfirm" />
        </c:set>
        <table class="basic">
            <thead>
                <tr>
                    <th>id</th>
                    <th><f:message key="stadium.city"/></th>
                    <th><f:message key="stadium.trainer"/></th>
                    <th><f:message key="stadium.type"/></th>
                    <c:if test="${actionBean.roleName == 'admin'}">
                        <th></th>
                        <th></th>
                    </c:if>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${actionBean.stadiums}" var="stadium">
                    <tr>
                        <td>${stadium.id}</td>
                        <td><c:out value="${stadium.city}"/></td>
                        <td><c:out value="${stadium.leader.name}"/></td>
                        <td><f:message key="pokemon.type.${stadium.type}"/></td>
                        <c:choose>
                            <c:when test="${actionBean.roleName == 'admin'}">
                                <td>
                                    <s:link beanclass="pa165.fi.muni.pokemon.StadiumsActionBean" event="edit">
                                        <s:param name="stadium.city" value="${stadium.city}"/>
                                        <f:message key="stadium.edit.edit"/>
                                    </s:link>
                                </td>
                                <td>
                                    <s:form beanclass="pa165.fi.muni.pokemon.StadiumsActionBean">
                                        <s:hidden name="stadium.city" value="${stadium.city}"/>
                                        <s:submit name="delete" onclick="return confirm('${pageScope.deleteconfirm}');">
                                            <f:message key="stadium.list.delete"/>
                                        </s:submit>
                                    </s:form>
                                </td>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                        
                        
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <c:if test="${actionBean.roleName == 'admin'}">
            <div id="mydiv">
                <s:form beanclass="pa165.fi.muni.pokemon.StadiumsActionBean">
                    <fieldset id="createfield">
                        <legend id="texts">
                            <f:message key="stadium.list.newstadium"/>
                        </legend>
                        <%@include file="form.jsp"%>
                        <s:submit class="myButton2" name="add">
                            <f:message key="stadium.list.createnew"/>
                        </s:submit>
                    </fieldset>
                </s:form>
            </div>
        </c:if>
    </s:layout-component>
</s:layout-render>