<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>

<script>
    $(function () {
        $("#b2").datepicker();
        $("#b2").datepicker("option", "dateFormat", "dd.mm.yy");
    });
</script>

<table id="t2">
    <tr>
        <th><s:label for="b1" name="stadium.city" /></th>
        <td><s:text id="b1" name="stadium.city" /></td>
    </tr>
    <tr>
        <th><s:label for="b2" name="stadium.leader.id"/></th>
        <td><s:select id="b2" name="stadium.leader.id"><s:options-collection collection="${actionBean.trainers}" label="name" value="id" /></s:select></td>
    </tr>
    <tr>
        <th><s:label for="b4" name="stadium.type"/></th>
        <td><s:select id="b4" name="stadium.type"><s:options-enumeration enum="pa165.fi.muni.pokemon.entity.Type" /></s:select></td>
    </tr>
</table>