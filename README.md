## **Readme** ##

Command line interface:
        [id] - mandatory parameter
        <leader> - optional parameter

- usage example:
        
        standard commands menu:

	'?' - shows available commands (help)

	'show trainer list' - produces list of trainers

	'show stadiums list' - produces list of stadiums

        'create trainer [name] [birth] [stadium]' - creates a new trainer with the provided data.
                ex: create 'trainer Misty 1995-05-10 Winterfell' - stadium is optional and will only assign it if the stadium does not already have a leader

	'create stadium [city] [type] [leader]' - creates a new stadium with the provided data.

	'delete trainer [id]' - deletes trainer with id=[id]

	'delete stadium [city]' - deletes stadium with city=[city]

        'trainer interface [id]' (example: trainer interface 1) - Enters trainer interface allowing to interface with trainer object

	'stadium interface [city]' (ex: stadium interface Winterfell) - Enters stadium interface allowing to interface with stadium object

        'exit' - exit CLI program

        under 'trainer interface [id]' are this commands:
            
            trainer set name [name] - change trainer name. Name has to be unique.

            trainer set birth [birth] - change trainer birth. Birth has to be in YYYY-MM-DD format.

            trainer set stadium [stadium city] - add/change trainer stadium leadership. Stadium has to be without leader.

            show trainer pokemon list - show trainer pokemons in list.

            show trainer badge list - show trainer badges in list.

            exit - go back to standard commands menu

        under 'stadium interface [city]' are this commands:

            stadium set type [type] - change stadium type (type from entity)

            stadium set leader [trainer id] - change/add stadium leader. trainer can't be already leader of different stadium.

            show stadium leader - show leader of stadium.

            exit - go back to standard commands menu

REST API
- usage:

		pa165/stadiums/

			GET: Produces list of stadiums in JSON
			example using curl:
				curl -i -X GET http://localhost:8080/pa165/rest/stadiums/

			POST: By providing a JSON value for stadium, creates a new stadium
			example using curl:
				curl -i -X POST -H "Content-Type:application/json" -d'{"city":"HEHE", "type":"FIRE", "leader":"1"}' http://localhost:8080/pa165/rest/stadiums

			PUT: Modifies stadium
			example using curl:
				curl -i -X PUT -H "Content-Type:application/json" -d'{"city":"HEHE", "type":"ICE"}' http://localhost:8080/pa165/rest/stadiums

		pa165/stadiums/{city}

			GET: Produces data about the stadium in JSON format
			example using curl:
				curl -i -X GET http://localhost:8080/pa165/rest/stadiums/Winterfell

			DELETE: Deletes stadium

		pa165/trainers/

			GET: Produces list of trainers in JSON

			POST: Creates new trainer. Input data - JSON

			PUT: Modifies selected trainer. Input data - JSON

		pa165/trainers/{id}

			GET: Produces data about a trainer with ID=id. Output: application/json

			DELETE: Deletes trainer

## **Final description** ##

K získání přístupu do pokémonové ligi musejí nejprve trenéři získat určitý počet odznaků ze stadionů. Odznak lze získat poražením vůdce stadionu, který je také trenérem. Aplikace bude sloužit k evidenci jednotlivých trenérů, odznaků které získali, stadionů a pokémonů náležících trenérům. Každý pokémon bude mít jméno, přezdívku, typ, úroveň a příslušnost ke konkrétnímu trenérovi. Každý stadion bude mít město ve kterém leží, typové zaměření a trenéra který je vůdcem stadionu. Odznak bude obsahovat stadion původu a trenéra, který jej získal. U jednotlivých trenérů pak bude uchováno jméno, příjmení a datum narození. Pokud je vůdce trenérem daného stadionu nesmí získat jeho odznak.

## **Preliminary description** ##

The project should emulate a real situation of pokémon world. Trainer wanders cities with stadiums and has some pokémons. He challenges masters of these stadiums who are also trainers with their own pokémons. Eventually when he defeats all stadiums available he can enter the pokémon league.

## **Use case diagram** ##
![UseCase.jpg](https://bitbucket.org/repo/qobkye/images/1157854697-UseCase.jpg)

## **Class diagram** ##
![UML.jpg](https://bitbucket.org/repo/qobkye/images/2423735369-UML.jpg)