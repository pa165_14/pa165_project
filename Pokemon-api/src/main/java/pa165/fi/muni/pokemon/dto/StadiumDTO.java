package pa165.fi.muni.pokemon.dto;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Petr
 */
public class StadiumDTO {

    private long id;

    private String city;

    private Type type;

    private TrainerDTO leader;

    private Set<BadgeDTO> sBadges = new HashSet<>();

    public StadiumDTO() {

    }

    public StadiumDTO(long id, String city, Type type, TrainerDTO leader) {
        this.id = id;
        this.city = city;
        this.type = type;
        this.leader = leader;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public TrainerDTO getLeader() {
        return leader;
    }

    public void setLeader(TrainerDTO leader) {
        this.leader = leader;
    }

    public Set<BadgeDTO> getsBadges() {
        return sBadges;
    }

    public void setsBadges(Set<BadgeDTO> sBadges) {
        this.sBadges = sBadges;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StadiumDTO other = (StadiumDTO) obj;
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        return this.type == other.type;
    }

    @Override
    public String toString() {
        return "StadiumTO{" + "id=" + id + ", city=" + city + ", type=" + type + ", leader=" + leader.getName() + '}';
    }
}
