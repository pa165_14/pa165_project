/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon.service;

import java.util.Date;
import java.util.List;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;

/**
 *
 * @author Kuba
 */
public interface TrainerService {

    public TrainerDTO create(String name, Date birth);

    public void delete(long id);

    public void update(TrainerDTO trainerDTO);

    public TrainerDTO getTrainerById(Long id);

    public TrainerDTO getTrainerByName(String name);

    public List<TrainerDTO> getAllTrainers();

    public int getCountBadges(TrainerDTO trainerDTO);

    public Boolean enterToLeague(TrainerDTO trainerDTO);

    public Boolean defeat(TrainerDTO trainer, StadiumDTO stadium);
}
