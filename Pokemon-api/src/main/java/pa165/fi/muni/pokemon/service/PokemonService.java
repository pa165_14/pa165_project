/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon.service;

import java.util.List;
import pa165.fi.muni.pokemon.dto.PokemonDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Kuba
 */
public interface PokemonService {

    public PokemonDTO addPokemon(PokemonDTO pokemonDTO);

    /**
     * Creates pokémon
     *
     * @param trainerDTO which will own created pokémon
     * @param name type name of the pokémon (e.g. Pikachu)
     * @param nickname nickname of the pokémon
     * @param type type of the pokémon (ice, fire, etc.)
     * @param lvl level of the pokémon
     * @return created pokémon
     */
    public PokemonDTO create(TrainerDTO trainerDTO, String name, String nickname, Type type, int lvl);

    /**
     * Deletes pokémon
     *
     * @param id id of pokémon to be deleted
     */
    public void delete(long id);

    /**
     * Updates pokémon
     *
     * @param pokemonDTO data of updated pokémon
     */
    public void update(PokemonDTO pokemonDTO);

    /**
     * Returns pokémon by its id
     *
     * @param id id of pokémon to be found
     * @return pokémon if found
     */
    public PokemonDTO getPokemonById(Long id);

    /**
     * Returns pokémon by its nickname
     *
     * @param name name of pokémon to be found
     * @return pokémon if found
     */
    public PokemonDTO getPokemonByNickname(String name);

    /**
     * Returns list of pokémons of a trainer
     *
     * @param trainerDTO owner of pokémons to be found
     * @return list of pokémons found
     */
    public List<PokemonDTO> getPokemonsOfTrainer(TrainerDTO trainerDTO);

    /**
     * Returns list of all pokémons
     *
     *
     * @return list of pokémons found
     */
    public List<PokemonDTO> getAllPokemons();
}
