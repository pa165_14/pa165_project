/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon.service;

import java.util.List;
import pa165.fi.muni.pokemon.dto.BadgeDTO;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;

/**
 *
 * @author Petr
 */
public interface BadgeService {

    /**
     * Creates badge
     *
     * @param trainerDTO which trainer will own the badge
     * @param stadiumDTO what stadium has the trainer defeated
     * @return created badge
     */
    public BadgeDTO create(TrainerDTO trainerDTO, StadiumDTO stadiumDTO);

    /**
     * Deletes badge
     *
     * @param id id of badge to be deleted
     */
    public void delete(long id);

    /**
     * Updates badge
     *
     * @param badgeDTO data of updated badge
     */
    public void update(BadgeDTO badgeDTO);

    /**
     * Returns all badges you can obtain.
     *
     * @param holderDTO Babges holder
     * @return list of all badges
     */
    public List<BadgeDTO> getAllTrainerBadges(TrainerDTO holderDTO);

    /**
     * Returns all types of badges you can obtain.
     *
     * @return list of all badges
     */
    public List<BadgeDTO> getAllBadges();

    /**
     * Returns count of badges you can obtain.
     *
     * @return count of all badges
     */
    public int countAllBadges();
}
