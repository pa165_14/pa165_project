package pa165.fi.muni.pokemon.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Kuba
 */
public class TrainerDTO {

    private long id;
    private String name;

    private Date birth;

    private Set<PokemonDTO> pokemons = new HashSet<>();

    private Set<BadgeDTO> badges = new HashSet<>();

    private StadiumDTO stadium;

    public TrainerDTO() {

    }

    public TrainerDTO(String name, Date birth, StadiumDTO stadium) {
        init(name, birth, stadium);
    }

    public TrainerDTO(TrainerDTO t) {
        init(t.name, t.birth, t.stadium);
    }

    private void init(String name, Date birth, StadiumDTO stadium) {
        this.name = name;
        this.birth = birth;
        this.stadium = stadium;
    }

    public TrainerDTO getData() {
        return new TrainerDTO(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public StadiumDTO getStadium() {
        return stadium;
    }

    public void setStadium(StadiumDTO stadium) {
        this.stadium = stadium;
    }

    public Set<PokemonDTO> getPokemons() {
        return pokemons;
    }

    public void setPokemons(Set<PokemonDTO> pokemons) {
        this.pokemons = pokemons;
    }

    public Set<BadgeDTO> getBadges() {
        return badges;
    }

    public void setBadges(Set<BadgeDTO> badges) {
        this.badges = badges;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrainerDTO other = (TrainerDTO) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return Objects.equals(this.birth, other.birth);
    }

    @Override
    public String toString() {
        return "Trainer: " + name + ", born: " + birth;
    }
}
