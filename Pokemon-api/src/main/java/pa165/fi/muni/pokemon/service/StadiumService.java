/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon.service;

import java.util.List;
import pa165.fi.muni.pokemon.dto.StadiumDTO;
import pa165.fi.muni.pokemon.dto.TrainerDTO;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Petr
 */
public interface StadiumService {

    /**
     * Creates stadium
     *
     * @param trainerDTO trainer who runs the stadium
     * @param city name of city in which stadium lies
     * @param type type of stadium (ice, fire, ...)
     * @return created stadium
     */
    public StadiumDTO create(TrainerDTO trainerDTO, String city, Type type);

    /**
     * Deletes stadium
     *
     * @param id id of stadium to be deleted
     */
    public void delete(long id);

    /**
     * Updates stadium
     *
     * @param stadiumDTO data of updated stadium
     */
    public void update(StadiumDTO stadiumDTO);

    /**
     * Tries to assign trainer to stadium and returns true if it was successful.
     *
     * @param trainerDTO trainer to assign to stadium
     * @param stadiumDTO stadium to be given the trainer
     * @return true if assigning was successful
     */
    public Boolean assignTrainerToStadium(TrainerDTO trainerDTO, StadiumDTO stadiumDTO);

    /**
     * Returns stadium by its city in which lies.
     *
     * @param city name of city in which stadium lies
     * @return stadium if found
     */
    public StadiumDTO getStadiumByCity(String city);

    /**
     * Returns list of all stadiums.
     *
     * @return list of all stadiums
     */
    public List<StadiumDTO> getAllStadiums();

    /**
     * Returns list of all stadiums by given type.
     *
     * @param type type of stadium (ice, fire, ...)
     * @return list of stadiums with given type
     */
    public List<StadiumDTO> getAllStadiumsByType(Type type);
}
