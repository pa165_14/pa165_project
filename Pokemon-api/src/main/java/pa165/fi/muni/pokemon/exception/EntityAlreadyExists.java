/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon.exception;

/**
 *
 * @author Petr
 */
public class EntityAlreadyExists extends RuntimeException {
    
    /**
     * Creates a new instance of <code>EntityAlreadyExists</code> without detail
     * message.
     */
    public EntityAlreadyExists()
    {
    }
    
    /**
     * Constructs an instance of <code>EntityAlreadyExists</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public EntityAlreadyExists(String msg)
    {
        super(msg);
    }

    /**
     * Constructs an instance of <code>EntityAlreadyExists</code> with the
     * specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause of the exception.
     */
    public EntityAlreadyExists(String message, Throwable cause)
    {
        super(message, cause);
    }
}
