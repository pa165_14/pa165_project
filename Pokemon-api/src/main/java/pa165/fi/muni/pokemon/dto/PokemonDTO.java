package pa165.fi.muni.pokemon.dto;

import java.util.Objects;
import pa165.fi.muni.pokemon.entity.Type;

/**
 *
 * @author Petr
 */
public class PokemonDTO {

    private long id;

    private String name;

    private String nickname;

    private Type type;

    private int lvl;

    private TrainerDTO owner;

    public PokemonDTO() {

    }

    public PokemonDTO(String name, Type type, TrainerDTO owner) {
        init(name, type, owner);
    }

    public PokemonDTO(PokemonDTO p) {
        init(p.name, p.type, p.owner);
    }

    private void init(String name, Type type, TrainerDTO owner) {
        this.name = name;
        this.type = type;
        this.owner = owner;
    }

    public PokemonDTO getData() {
        return new PokemonDTO(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public TrainerDTO getOwner() {
        return owner;
    }

    public void setOwner(TrainerDTO owner) {
        this.owner = owner;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 61 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PokemonDTO other = (PokemonDTO) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return Objects.equals(this.owner, other.owner);
    }

    @Override
    public String toString() {
        return "PokemonTO{" + "id=" + id + ", name=" + name + ", nickname=" + nickname + ", type=" + type + ", lvl=" + lvl + ", owner=" + owner.getName() + '}';
    }
}
