package pa165.fi.muni.pokemon.dto;

import java.util.Objects;

/**
 *
 * @author Petr
 */
public class BadgeDTO {

    private long id;

    private TrainerDTO holder;

    private StadiumDTO stadium;

    public BadgeDTO() {

    }

    public BadgeDTO(long id, TrainerDTO holder, StadiumDTO stadium) {
        this.id = id;
        this.holder = holder;
        this.stadium = stadium;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TrainerDTO getHolder() {
        return holder;
    }

    public void setHolder(TrainerDTO holder) {
        this.holder = holder;
    }

    public StadiumDTO getStadium() {
        return stadium;
    }

    public void setStadium(StadiumDTO stadium) {
        this.stadium = stadium;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BadgeDTO other = (BadgeDTO) obj;
        if (!Objects.equals(this.holder, other.holder)) {
            return false;
        }
        return Objects.equals(this.stadium, other.stadium);
    }

    @Override
    public String toString() {
        return "BadgeTO{" + "id=" + id + ", holder=" + holder.getName() + ", stadium=" + stadium.getCity() + '}';
    }
}
