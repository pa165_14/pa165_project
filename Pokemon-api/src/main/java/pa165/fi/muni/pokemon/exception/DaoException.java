/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa165.fi.muni.pokemon.exception;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author Petr
 */
@Aspect
@Component
public class DaoException extends RuntimeException {

    @AfterThrowing(pointcut = "within(pa165.fi.muni.pokemon.dao.*)", throwing = "ex")
    public void afterThrowingExecution(JoinPoint jp, Exception ex) {
        throw new DataAccessException("Some error occured in dao method.", ex) {
        };
    }
}
